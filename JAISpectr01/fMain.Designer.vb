﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fMain
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea5 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend5 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series5 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea6 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend6 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series6 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.bEnd = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lCamID1 = New System.Windows.Forms.Label()
        Me.lCamID2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.bConnect = New System.Windows.Forms.Button()
        Me.ldebug = New System.Windows.Forms.Label()
        Me.lDebug2 = New System.Windows.Forms.Label()
        Me.bVideo1 = New System.Windows.Forms.Button()
        Me.bStopVideo1 = New System.Windows.Forms.Button()
        Me.bStopVideo2 = New System.Windows.Forms.Button()
        Me.bVideo2 = New System.Windows.Forms.Button()
        Me.bSaveSetting = New System.Windows.Forms.Button()
        Me.ALCTypeComboBox = New System.Windows.Forms.ComboBox()
        Me.StopButton = New System.Windows.Forms.Button()
        Me.cbSave = New System.Windows.Forms.CheckBox()
        Me.cbAutoExp = New System.Windows.Forms.CheckBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.gbALC = New System.Windows.Forms.GroupBox()
        Me.setpointTrackBar = New System.Windows.Forms.TrackBar()
        Me.setpointTextBox = New System.Windows.Forms.TextBox()
        Me.gAutoALC = New System.Windows.Forms.GroupBox()
        Me.exposureTextBox = New System.Windows.Forms.TextBox()
        Me.gainTextBox = New System.Windows.Forms.TextBox()
        Me.ExposureGroupBox = New System.Windows.Forms.GroupBox()
        Me.lExposure = New System.Windows.Forms.Label()
        Me.ExposureTrackBar = New System.Windows.Forms.TrackBar()
        Me.GainGroupBox = New System.Windows.Forms.GroupBox()
        Me.GainLabel = New System.Windows.Forms.Label()
        Me.GainTrackBar = New System.Windows.Forms.TrackBar()
        Me.cbUsePeak = New System.Windows.Forms.CheckBox()
        Me.lExpData = New System.Windows.Forms.Label()
        Me.TimerAcq = New System.Windows.Forms.Timer(Me.components)
        Me.bAutoAcq = New System.Windows.Forms.Button()
        Me.lDebug3 = New System.Windows.Forms.Label()
        Me.gbHidden = New System.Windows.Forms.GroupBox()
        Me.groupBox5 = New System.Windows.Forms.GroupBox()
        Me.roiheightNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.roiwidthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.yposNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.xposNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.label17 = New System.Windows.Forms.Label()
        Me.label16 = New System.Windows.Forms.Label()
        Me.label15 = New System.Windows.Forms.Label()
        Me.label14 = New System.Windows.Forms.Label()
        Me.groupBox4 = New System.Windows.Forms.GroupBox()
        Me.maxExposureNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.maxGainNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.minExposureNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.minGainNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.label13 = New System.Windows.Forms.Label()
        Me.label12 = New System.Windows.Forms.Label()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.ImageSizeGroupBox = New System.Windows.Forms.GroupBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.HeightNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.WidthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.cbUD1 = New System.Windows.Forms.CheckBox()
        Me.cbUD2 = New System.Windows.Forms.CheckBox()
        Me.setupButton1 = New System.Windows.Forms.Button()
        Me.lCounter = New System.Windows.Forms.Label()
        Me.LCamRecording = New System.Windows.Forms.Label()
        Me.lStatus = New System.Windows.Forms.Label()
        Me.PanelLine = New System.Windows.Forms.Panel()
        Me.gbSampling = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.exposureUpDown = New System.Windows.Forms.NumericUpDown()
        Me.YellowLinePanel2 = New System.Windows.Forms.Panel()
        Me.VerticalLine2 = New System.Windows.Forms.Panel()
        Me.YellowLinePanel1 = New System.Windows.Forms.Panel()
        Me.lSaveFolder = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.bTest = New System.Windows.Forms.Button()
        Me.lSpectrRecording = New System.Windows.Forms.Label()
        Me.lMessage = New System.Windows.Forms.Label()
        Me.lSpectrCounter = New System.Windows.Forms.Label()
        Me.cbSaveSpectrums = New System.Windows.Forms.CheckBox()
        Me.LEDconnected = New System.Windows.Forms.Button()
        Me.lSpectrConnected = New System.Windows.Forms.Label()
        Me.bClear = New System.Windows.Forms.Button()
        Me.bDark = New System.Windows.Forms.Button()
        Me.bStopSpectrAcq = New System.Windows.Forms.Button()
        Me.bConnectSpectrometer = New System.Windows.Forms.Button()
        Me.bContinuous = New System.Windows.Forms.Button()
        Me.bGetSpectrum = New System.Windows.Forms.Button()
        Me.gUSB2000 = New System.Windows.Forms.GroupBox()
        Me.LEDSpectrOpt = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.NumExpXsec = New System.Windows.Forms.NumericUpDown()
        Me.cbRestrictBand = New System.Windows.Forms.CheckBox()
        Me.cbAutoSpectr = New System.Windows.Forms.CheckBox()
        Me.lOptimal = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.NumScans = New System.Windows.Forms.NumericUpDown()
        Me.NumIntTime = New System.Windows.Forms.NumericUpDown()
        Me.USB2000chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.RatioChart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TimerSpectr = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSLtemperature = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lToolStripDebug1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lToolStripDebug2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripErrors = New System.Windows.Forms.ToolStripStatusLabel()
        Me.cbLongExp = New System.Windows.Forms.CheckBox()
        Me.lLongExposure = New System.Windows.Forms.Label()
        Me.LtimerAcq = New System.Windows.Forms.Label()
        Me.Ltimer1 = New System.Windows.Forms.Label()
        Me.TimerDebug = New System.Windows.Forms.Timer(Me.components)
        Me.lDebug4 = New System.Windows.Forms.Label()
        Me.lDebug5 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbALC.SuspendLayout()
        CType(Me.setpointTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gAutoALC.SuspendLayout()
        Me.ExposureGroupBox.SuspendLayout()
        CType(Me.ExposureTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GainGroupBox.SuspendLayout()
        CType(Me.GainTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbHidden.SuspendLayout()
        Me.groupBox5.SuspendLayout()
        CType(Me.roiheightNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.roiwidthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.yposNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xposNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBox4.SuspendLayout()
        CType(Me.maxExposureNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.maxGainNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.minExposureNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.minGainNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ImageSizeGroupBox.SuspendLayout()
        CType(Me.HeightNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WidthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSampling.SuspendLayout()
        CType(Me.exposureUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gUSB2000.SuspendLayout()
        CType(Me.NumExpXsec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumScans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumIntTime, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.USB2000chart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RatioChart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(4, 33)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(502, 375)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Black
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox2.Location = New System.Drawing.Point(512, 33)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(502, 375)
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'bEnd
        '
        Me.bEnd.Location = New System.Drawing.Point(1241, 357)
        Me.bEnd.Name = "bEnd"
        Me.bEnd.Size = New System.Drawing.Size(50, 32)
        Me.bEnd.TabIndex = 2
        Me.bEnd.Text = "End"
        Me.bEnd.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Silver
        Me.Label1.Location = New System.Drawing.Point(1, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Camera 1 310"
        '
        'lCamID1
        '
        Me.lCamID1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lCamID1.Location = New System.Drawing.Point(81, 5)
        Me.lCamID1.Name = "lCamID1"
        Me.lCamID1.Size = New System.Drawing.Size(90, 20)
        Me.lCamID1.TabIndex = 4
        Me.lCamID1.Text = "Cam ID 1"
        '
        'lCamID2
        '
        Me.lCamID2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lCamID2.Location = New System.Drawing.Point(594, 6)
        Me.lCamID2.Name = "lCamID2"
        Me.lCamID2.Size = New System.Drawing.Size(90, 20)
        Me.lCamID2.TabIndex = 6
        Me.lCamID2.Text = "Cam ID 2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(515, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Camera 2 330"
        '
        'bConnect
        '
        Me.bConnect.Location = New System.Drawing.Point(1017, 273)
        Me.bConnect.Name = "bConnect"
        Me.bConnect.Size = New System.Drawing.Size(81, 32)
        Me.bConnect.TabIndex = 7
        Me.bConnect.Text = "Connect"
        Me.bConnect.UseVisualStyleBackColor = True
        '
        'ldebug
        '
        Me.ldebug.AutoSize = True
        Me.ldebug.Location = New System.Drawing.Point(1226, 12)
        Me.ldebug.Name = "ldebug"
        Me.ldebug.Size = New System.Drawing.Size(41, 13)
        Me.ldebug.TabIndex = 8
        Me.ldebug.Text = "lDebug"
        '
        'lDebug2
        '
        Me.lDebug2.AutoSize = True
        Me.lDebug2.Location = New System.Drawing.Point(1226, 29)
        Me.lDebug2.Name = "lDebug2"
        Me.lDebug2.Size = New System.Drawing.Size(47, 13)
        Me.lDebug2.TabIndex = 9
        Me.lDebug2.Text = "lDebug2"
        '
        'bVideo1
        '
        Me.bVideo1.Location = New System.Drawing.Point(1223, 426)
        Me.bVideo1.Name = "bVideo1"
        Me.bVideo1.Size = New System.Drawing.Size(75, 23)
        Me.bVideo1.TabIndex = 10
        Me.bVideo1.Text = "Video1"
        Me.bVideo1.UseVisualStyleBackColor = True
        '
        'bStopVideo1
        '
        Me.bStopVideo1.Location = New System.Drawing.Point(1223, 456)
        Me.bStopVideo1.Name = "bStopVideo1"
        Me.bStopVideo1.Size = New System.Drawing.Size(75, 23)
        Me.bStopVideo1.TabIndex = 11
        Me.bStopVideo1.Text = "StopVideo1"
        Me.bStopVideo1.UseVisualStyleBackColor = True
        '
        'bStopVideo2
        '
        Me.bStopVideo2.Location = New System.Drawing.Point(1223, 515)
        Me.bStopVideo2.Name = "bStopVideo2"
        Me.bStopVideo2.Size = New System.Drawing.Size(75, 23)
        Me.bStopVideo2.TabIndex = 13
        Me.bStopVideo2.Text = "StopVideo2"
        Me.bStopVideo2.UseVisualStyleBackColor = True
        '
        'bVideo2
        '
        Me.bVideo2.Location = New System.Drawing.Point(1223, 485)
        Me.bVideo2.Name = "bVideo2"
        Me.bVideo2.Size = New System.Drawing.Size(75, 23)
        Me.bVideo2.TabIndex = 12
        Me.bVideo2.Text = "Video2"
        Me.bVideo2.UseVisualStyleBackColor = True
        '
        'bSaveSetting
        '
        Me.bSaveSetting.DialogResult = System.Windows.Forms.DialogResult.Abort
        Me.bSaveSetting.Location = New System.Drawing.Point(1017, 216)
        Me.bSaveSetting.Name = "bSaveSetting"
        Me.bSaveSetting.Size = New System.Drawing.Size(98, 25)
        Me.bSaveSetting.TabIndex = 14
        Me.bSaveSetting.Text = "Save settings"
        Me.bSaveSetting.UseVisualStyleBackColor = True
        '
        'ALCTypeComboBox
        '
        Me.ALCTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ALCTypeComboBox.FormattingEnabled = True
        Me.ALCTypeComboBox.Items.AddRange(New Object() {"Minimum Gain Priority", "Minimum Exposure Priority", "Gain Priority", "Exposure Priority", "Gain Only", "Exposure Only"})
        Me.ALCTypeComboBox.Location = New System.Drawing.Point(1121, 218)
        Me.ALCTypeComboBox.Name = "ALCTypeComboBox"
        Me.ALCTypeComboBox.Size = New System.Drawing.Size(172, 21)
        Me.ALCTypeComboBox.TabIndex = 16
        '
        'StopButton
        '
        Me.StopButton.Enabled = False
        Me.StopButton.Location = New System.Drawing.Point(1174, 273)
        Me.StopButton.Name = "StopButton"
        Me.StopButton.Size = New System.Drawing.Size(50, 32)
        Me.StopButton.TabIndex = 53
        Me.StopButton.Text = "Stop"
        Me.StopButton.UseVisualStyleBackColor = True
        '
        'cbSave
        '
        Me.cbSave.AutoSize = True
        Me.cbSave.Location = New System.Drawing.Point(1017, 345)
        Me.cbSave.Name = "cbSave"
        Me.cbSave.Size = New System.Drawing.Size(87, 17)
        Me.cbSave.TabIndex = 54
        Me.cbSave.Text = "Save images"
        Me.cbSave.UseVisualStyleBackColor = True
        '
        'cbAutoExp
        '
        Me.cbAutoExp.AutoSize = True
        Me.cbAutoExp.Location = New System.Drawing.Point(1017, 311)
        Me.cbAutoExp.Name = "cbAutoExp"
        Me.cbAutoExp.Size = New System.Drawing.Size(69, 17)
        Me.cbAutoExp.TabIndex = 55
        Me.cbAutoExp.Text = "Auto Exp"
        Me.cbAutoExp.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10
        '
        'gbALC
        '
        Me.gbALC.Controls.Add(Me.setpointTrackBar)
        Me.gbALC.Controls.Add(Me.setpointTextBox)
        Me.gbALC.Controls.Add(Me.gAutoALC)
        Me.gbALC.Controls.Add(Me.ExposureGroupBox)
        Me.gbALC.Controls.Add(Me.GainGroupBox)
        Me.gbALC.Location = New System.Drawing.Point(1017, 65)
        Me.gbALC.Name = "gbALC"
        Me.gbALC.Size = New System.Drawing.Size(276, 151)
        Me.gbALC.TabIndex = 56
        Me.gbALC.TabStop = False
        Me.gbALC.Text = "ALC"
        '
        'setpointTrackBar
        '
        Me.setpointTrackBar.Location = New System.Drawing.Point(0, 106)
        Me.setpointTrackBar.Name = "setpointTrackBar"
        Me.setpointTrackBar.Size = New System.Drawing.Size(194, 45)
        Me.setpointTrackBar.TabIndex = 53
        Me.setpointTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both
        '
        'setpointTextBox
        '
        Me.setpointTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.setpointTextBox.Location = New System.Drawing.Point(212, 117)
        Me.setpointTextBox.Name = "setpointTextBox"
        Me.setpointTextBox.ReadOnly = True
        Me.setpointTextBox.Size = New System.Drawing.Size(50, 20)
        Me.setpointTextBox.TabIndex = 54
        Me.setpointTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gAutoALC
        '
        Me.gAutoALC.Controls.Add(Me.exposureTextBox)
        Me.gAutoALC.Controls.Add(Me.gainTextBox)
        Me.gAutoALC.Location = New System.Drawing.Point(206, 15)
        Me.gAutoALC.Name = "gAutoALC"
        Me.gAutoALC.Size = New System.Drawing.Size(65, 93)
        Me.gAutoALC.TabIndex = 52
        Me.gAutoALC.TabStop = False
        Me.gAutoALC.Text = "AutoALC"
        '
        'exposureTextBox
        '
        Me.exposureTextBox.Location = New System.Drawing.Point(6, 69)
        Me.exposureTextBox.Name = "exposureTextBox"
        Me.exposureTextBox.ReadOnly = True
        Me.exposureTextBox.Size = New System.Drawing.Size(50, 20)
        Me.exposureTextBox.TabIndex = 35
        Me.exposureTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gainTextBox
        '
        Me.gainTextBox.Location = New System.Drawing.Point(6, 18)
        Me.gainTextBox.Name = "gainTextBox"
        Me.gainTextBox.ReadOnly = True
        Me.gainTextBox.Size = New System.Drawing.Size(50, 20)
        Me.gainTextBox.TabIndex = 34
        Me.gainTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ExposureGroupBox
        '
        Me.ExposureGroupBox.Controls.Add(Me.lExposure)
        Me.ExposureGroupBox.Controls.Add(Me.ExposureTrackBar)
        Me.ExposureGroupBox.Location = New System.Drawing.Point(0, 62)
        Me.ExposureGroupBox.Name = "ExposureGroupBox"
        Me.ExposureGroupBox.Size = New System.Drawing.Size(200, 45)
        Me.ExposureGroupBox.TabIndex = 51
        Me.ExposureGroupBox.TabStop = False
        Me.ExposureGroupBox.Text = "Exposure Control"
        '
        'lExposure
        '
        Me.lExposure.AutoSize = True
        Me.lExposure.Enabled = False
        Me.lExposure.Location = New System.Drawing.Point(6, 16)
        Me.lExposure.Name = "lExposure"
        Me.lExposure.Size = New System.Drawing.Size(13, 13)
        Me.lExposure.TabIndex = 2
        Me.lExposure.Text = "0"
        '
        'ExposureTrackBar
        '
        Me.ExposureTrackBar.Enabled = False
        Me.ExposureTrackBar.Location = New System.Drawing.Point(25, 16)
        Me.ExposureTrackBar.Name = "ExposureTrackBar"
        Me.ExposureTrackBar.Size = New System.Drawing.Size(169, 45)
        Me.ExposureTrackBar.TabIndex = 1
        '
        'GainGroupBox
        '
        Me.GainGroupBox.Controls.Add(Me.GainLabel)
        Me.GainGroupBox.Controls.Add(Me.GainTrackBar)
        Me.GainGroupBox.Location = New System.Drawing.Point(0, 15)
        Me.GainGroupBox.Name = "GainGroupBox"
        Me.GainGroupBox.Size = New System.Drawing.Size(200, 45)
        Me.GainGroupBox.TabIndex = 50
        Me.GainGroupBox.TabStop = False
        Me.GainGroupBox.Text = "Gain Control"
        '
        'GainLabel
        '
        Me.GainLabel.AutoSize = True
        Me.GainLabel.Enabled = False
        Me.GainLabel.Location = New System.Drawing.Point(6, 16)
        Me.GainLabel.Name = "GainLabel"
        Me.GainLabel.Size = New System.Drawing.Size(13, 13)
        Me.GainLabel.TabIndex = 2
        Me.GainLabel.Text = "0"
        '
        'GainTrackBar
        '
        Me.GainTrackBar.Enabled = False
        Me.GainTrackBar.Location = New System.Drawing.Point(25, 16)
        Me.GainTrackBar.Name = "GainTrackBar"
        Me.GainTrackBar.Size = New System.Drawing.Size(169, 45)
        Me.GainTrackBar.TabIndex = 1
        '
        'cbUsePeak
        '
        Me.cbUsePeak.AutoSize = True
        Me.cbUsePeak.Location = New System.Drawing.Point(1017, 328)
        Me.cbUsePeak.Name = "cbUsePeak"
        Me.cbUsePeak.Size = New System.Drawing.Size(73, 17)
        Me.cbUsePeak.TabIndex = 57
        Me.cbUsePeak.Text = "Use Peak"
        Me.cbUsePeak.UseVisualStyleBackColor = True
        '
        'lExpData
        '
        Me.lExpData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lExpData.Location = New System.Drawing.Point(1017, 245)
        Me.lExpData.Name = "lExpData"
        Me.lExpData.Size = New System.Drawing.Size(276, 25)
        Me.lExpData.TabIndex = 58
        Me.lExpData.Text = "Exp data"
        '
        'TimerAcq
        '
        Me.TimerAcq.Interval = 500
        '
        'bAutoAcq
        '
        Me.bAutoAcq.Location = New System.Drawing.Point(1104, 273)
        Me.bAutoAcq.Name = "bAutoAcq"
        Me.bAutoAcq.Size = New System.Drawing.Size(64, 32)
        Me.bAutoAcq.TabIndex = 59
        Me.bAutoAcq.Text = "Start"
        Me.bAutoAcq.UseVisualStyleBackColor = True
        '
        'lDebug3
        '
        Me.lDebug3.AutoSize = True
        Me.lDebug3.Location = New System.Drawing.Point(1226, 47)
        Me.lDebug3.Name = "lDebug3"
        Me.lDebug3.Size = New System.Drawing.Size(47, 13)
        Me.lDebug3.TabIndex = 61
        Me.lDebug3.Text = "lDebug3"
        '
        'gbHidden
        '
        Me.gbHidden.Controls.Add(Me.groupBox5)
        Me.gbHidden.Controls.Add(Me.groupBox4)
        Me.gbHidden.Controls.Add(Me.ImageSizeGroupBox)
        Me.gbHidden.Location = New System.Drawing.Point(161, 139)
        Me.gbHidden.Name = "gbHidden"
        Me.gbHidden.Size = New System.Drawing.Size(652, 98)
        Me.gbHidden.TabIndex = 62
        Me.gbHidden.TabStop = False
        Me.gbHidden.Text = "Hidden"
        Me.gbHidden.Visible = False
        '
        'groupBox5
        '
        Me.groupBox5.Controls.Add(Me.roiheightNumericUpDown)
        Me.groupBox5.Controls.Add(Me.roiwidthNumericUpDown)
        Me.groupBox5.Controls.Add(Me.yposNumericUpDown)
        Me.groupBox5.Controls.Add(Me.xposNumericUpDown)
        Me.groupBox5.Controls.Add(Me.label17)
        Me.groupBox5.Controls.Add(Me.label16)
        Me.groupBox5.Controls.Add(Me.label15)
        Me.groupBox5.Controls.Add(Me.label14)
        Me.groupBox5.Location = New System.Drawing.Point(401, 33)
        Me.groupBox5.Name = "groupBox5"
        Me.groupBox5.Size = New System.Drawing.Size(213, 78)
        Me.groupBox5.TabIndex = 55
        Me.groupBox5.TabStop = False
        Me.groupBox5.Text = "Measurement ROI"
        '
        'roiheightNumericUpDown
        '
        Me.roiheightNumericUpDown.Location = New System.Drawing.Point(147, 49)
        Me.roiheightNumericUpDown.Name = "roiheightNumericUpDown"
        Me.roiheightNumericUpDown.ReadOnly = True
        Me.roiheightNumericUpDown.Size = New System.Drawing.Size(60, 20)
        Me.roiheightNumericUpDown.TabIndex = 8
        Me.roiheightNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'roiwidthNumericUpDown
        '
        Me.roiwidthNumericUpDown.Location = New System.Drawing.Point(147, 19)
        Me.roiwidthNumericUpDown.Name = "roiwidthNumericUpDown"
        Me.roiwidthNumericUpDown.ReadOnly = True
        Me.roiwidthNumericUpDown.Size = New System.Drawing.Size(60, 20)
        Me.roiwidthNumericUpDown.TabIndex = 7
        Me.roiwidthNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'yposNumericUpDown
        '
        Me.yposNumericUpDown.Location = New System.Drawing.Point(41, 49)
        Me.yposNumericUpDown.Name = "yposNumericUpDown"
        Me.yposNumericUpDown.ReadOnly = True
        Me.yposNumericUpDown.Size = New System.Drawing.Size(60, 20)
        Me.yposNumericUpDown.TabIndex = 6
        Me.yposNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'xposNumericUpDown
        '
        Me.xposNumericUpDown.Location = New System.Drawing.Point(41, 19)
        Me.xposNumericUpDown.Name = "xposNumericUpDown"
        Me.xposNumericUpDown.ReadOnly = True
        Me.xposNumericUpDown.Size = New System.Drawing.Size(58, 20)
        Me.xposNumericUpDown.TabIndex = 5
        Me.xposNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label17
        '
        Me.label17.AutoSize = True
        Me.label17.Location = New System.Drawing.Point(105, 51)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(41, 13)
        Me.label17.TabIndex = 3
        Me.label17.Text = "Height:"
        '
        'label16
        '
        Me.label16.AutoSize = True
        Me.label16.Location = New System.Drawing.Point(105, 21)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(38, 13)
        Me.label16.TabIndex = 2
        Me.label16.Text = "Width:"
        '
        'label15
        '
        Me.label15.AutoSize = True
        Me.label15.Location = New System.Drawing.Point(4, 51)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(37, 13)
        Me.label15.TabIndex = 1
        Me.label15.Text = "Y-pos:"
        '
        'label14
        '
        Me.label14.AutoSize = True
        Me.label14.Location = New System.Drawing.Point(4, 21)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(37, 13)
        Me.label14.TabIndex = 0
        Me.label14.Text = "X-pos:"
        '
        'groupBox4
        '
        Me.groupBox4.Controls.Add(Me.maxExposureNumericUpDown)
        Me.groupBox4.Controls.Add(Me.maxGainNumericUpDown)
        Me.groupBox4.Controls.Add(Me.minExposureNumericUpDown)
        Me.groupBox4.Controls.Add(Me.minGainNumericUpDown)
        Me.groupBox4.Controls.Add(Me.label13)
        Me.groupBox4.Controls.Add(Me.label12)
        Me.groupBox4.Controls.Add(Me.label11)
        Me.groupBox4.Controls.Add(Me.label10)
        Me.groupBox4.Location = New System.Drawing.Point(117, 32)
        Me.groupBox4.Name = "groupBox4"
        Me.groupBox4.Size = New System.Drawing.Size(278, 79)
        Me.groupBox4.TabIndex = 54
        Me.groupBox4.TabStop = False
        Me.groupBox4.Text = "Gain- and Exposure Time limits"
        '
        'maxExposureNumericUpDown
        '
        Me.maxExposureNumericUpDown.Location = New System.Drawing.Point(203, 47)
        Me.maxExposureNumericUpDown.Name = "maxExposureNumericUpDown"
        Me.maxExposureNumericUpDown.ReadOnly = True
        Me.maxExposureNumericUpDown.Size = New System.Drawing.Size(58, 20)
        Me.maxExposureNumericUpDown.TabIndex = 7
        Me.maxExposureNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'maxGainNumericUpDown
        '
        Me.maxGainNumericUpDown.Location = New System.Drawing.Point(203, 20)
        Me.maxGainNumericUpDown.Name = "maxGainNumericUpDown"
        Me.maxGainNumericUpDown.ReadOnly = True
        Me.maxGainNumericUpDown.Size = New System.Drawing.Size(60, 20)
        Me.maxGainNumericUpDown.TabIndex = 6
        Me.maxGainNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'minExposureNumericUpDown
        '
        Me.minExposureNumericUpDown.Location = New System.Drawing.Point(79, 49)
        Me.minExposureNumericUpDown.Name = "minExposureNumericUpDown"
        Me.minExposureNumericUpDown.ReadOnly = True
        Me.minExposureNumericUpDown.Size = New System.Drawing.Size(44, 20)
        Me.minExposureNumericUpDown.TabIndex = 5
        Me.minExposureNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'minGainNumericUpDown
        '
        Me.minGainNumericUpDown.Location = New System.Drawing.Point(79, 19)
        Me.minGainNumericUpDown.Name = "minGainNumericUpDown"
        Me.minGainNumericUpDown.ReadOnly = True
        Me.minGainNumericUpDown.Size = New System.Drawing.Size(44, 20)
        Me.minGainNumericUpDown.TabIndex = 4
        Me.minGainNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label13
        '
        Me.label13.AutoSize = True
        Me.label13.Location = New System.Drawing.Point(129, 50)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(77, 13)
        Me.label13.TabIndex = 3
        Me.label13.Text = "Max Exposure:"
        '
        'label12
        '
        Me.label12.AutoSize = True
        Me.label12.Location = New System.Drawing.Point(4, 51)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(74, 13)
        Me.label12.TabIndex = 2
        Me.label12.Text = "Min Exposure:"
        '
        'label11
        '
        Me.label11.AutoSize = True
        Me.label11.Location = New System.Drawing.Point(129, 21)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(55, 13)
        Me.label11.TabIndex = 1
        Me.label11.Text = "Max Gain:"
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.Location = New System.Drawing.Point(4, 21)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(52, 13)
        Me.label10.TabIndex = 0
        Me.label10.Text = "Min Gain:"
        '
        'ImageSizeGroupBox
        '
        Me.ImageSizeGroupBox.Controls.Add(Me.label2)
        Me.ImageSizeGroupBox.Controls.Add(Me.Label4)
        Me.ImageSizeGroupBox.Controls.Add(Me.HeightNumericUpDown)
        Me.ImageSizeGroupBox.Controls.Add(Me.WidthNumericUpDown)
        Me.ImageSizeGroupBox.Location = New System.Drawing.Point(-2, 31)
        Me.ImageSizeGroupBox.Name = "ImageSizeGroupBox"
        Me.ImageSizeGroupBox.Size = New System.Drawing.Size(113, 79)
        Me.ImageSizeGroupBox.TabIndex = 53
        Me.ImageSizeGroupBox.TabStop = False
        Me.ImageSizeGroupBox.Text = "Image Size"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(7, 50)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(38, 13)
        Me.label2.TabIndex = 6
        Me.label2.Text = "Height"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Width"
        '
        'HeightNumericUpDown
        '
        Me.HeightNumericUpDown.Enabled = False
        Me.HeightNumericUpDown.Location = New System.Drawing.Point(48, 49)
        Me.HeightNumericUpDown.Name = "HeightNumericUpDown"
        Me.HeightNumericUpDown.Size = New System.Drawing.Size(59, 20)
        Me.HeightNumericUpDown.TabIndex = 1
        '
        'WidthNumericUpDown
        '
        Me.WidthNumericUpDown.Enabled = False
        Me.WidthNumericUpDown.Location = New System.Drawing.Point(48, 21)
        Me.WidthNumericUpDown.Name = "WidthNumericUpDown"
        Me.WidthNumericUpDown.Size = New System.Drawing.Size(59, 20)
        Me.WidthNumericUpDown.TabIndex = 0
        '
        'cbUD1
        '
        Me.cbUD1.AutoSize = True
        Me.cbUD1.Enabled = False
        Me.cbUD1.Location = New System.Drawing.Point(179, 5)
        Me.cbUD1.Name = "cbUD1"
        Me.cbUD1.Size = New System.Drawing.Size(87, 17)
        Me.cbUD1.TabIndex = 63
        Me.cbUD1.Text = "UpsideDown"
        Me.cbUD1.UseVisualStyleBackColor = True
        Me.cbUD1.Visible = False
        '
        'cbUD2
        '
        Me.cbUD2.AutoSize = True
        Me.cbUD2.Enabled = False
        Me.cbUD2.Location = New System.Drawing.Point(688, 5)
        Me.cbUD2.Name = "cbUD2"
        Me.cbUD2.Size = New System.Drawing.Size(87, 17)
        Me.cbUD2.TabIndex = 64
        Me.cbUD2.Text = "UpsideDown"
        Me.cbUD2.UseVisualStyleBackColor = True
        Me.cbUD2.Visible = False
        '
        'setupButton1
        '
        Me.setupButton1.Location = New System.Drawing.Point(1017, 7)
        Me.setupButton1.Name = "setupButton1"
        Me.setupButton1.Size = New System.Drawing.Size(75, 23)
        Me.setupButton1.TabIndex = 66
        Me.setupButton1.Text = "Setup"
        Me.setupButton1.UseVisualStyleBackColor = True
        '
        'lCounter
        '
        Me.lCounter.AutoSize = True
        Me.lCounter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lCounter.Location = New System.Drawing.Point(1230, 290)
        Me.lCounter.Name = "lCounter"
        Me.lCounter.Size = New System.Drawing.Size(46, 15)
        Me.lCounter.TabIndex = 67
        Me.lCounter.Text = "Counter"
        '
        'LCamRecording
        '
        Me.LCamRecording.AutoSize = True
        Me.LCamRecording.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCamRecording.ForeColor = System.Drawing.Color.LimeGreen
        Me.LCamRecording.Location = New System.Drawing.Point(1119, 364)
        Me.LCamRecording.Name = "LCamRecording"
        Me.LCamRecording.Size = New System.Drawing.Size(108, 16)
        Me.LCamRecording.TabIndex = 74
        Me.LCamRecording.Text = "Not Recording"
        '
        'lStatus
        '
        Me.lStatus.AutoSize = True
        Me.lStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lStatus.ForeColor = System.Drawing.Color.DarkOliveGreen
        Me.lStatus.Location = New System.Drawing.Point(1021, 365)
        Me.lStatus.Name = "lStatus"
        Me.lStatus.Size = New System.Drawing.Size(40, 16)
        Me.lStatus.TabIndex = 73
        Me.lStatus.Text = "Stop"
        '
        'PanelLine
        '
        Me.PanelLine.BackColor = System.Drawing.Color.DimGray
        Me.PanelLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PanelLine.ForeColor = System.Drawing.Color.DimGray
        Me.PanelLine.Location = New System.Drawing.Point(4, 410)
        Me.PanelLine.Name = "PanelLine"
        Me.PanelLine.Size = New System.Drawing.Size(1289, 10)
        Me.PanelLine.TabIndex = 76
        '
        'gbSampling
        '
        Me.gbSampling.Controls.Add(Me.Label5)
        Me.gbSampling.Controls.Add(Me.exposureUpDown)
        Me.gbSampling.Location = New System.Drawing.Point(1098, 7)
        Me.gbSampling.Name = "gbSampling"
        Me.gbSampling.Size = New System.Drawing.Size(119, 35)
        Me.gbSampling.TabIndex = 77
        Me.gbSampling.TabStop = False
        Me.gbSampling.Text = "Sampling interval"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(70, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "seconds"
        '
        'exposureUpDown
        '
        Me.exposureUpDown.DecimalPlaces = 3
        Me.exposureUpDown.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.exposureUpDown.Location = New System.Drawing.Point(6, 13)
        Me.exposureUpDown.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.exposureUpDown.Name = "exposureUpDown"
        Me.exposureUpDown.Size = New System.Drawing.Size(64, 20)
        Me.exposureUpDown.TabIndex = 13
        Me.exposureUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.exposureUpDown.Value = New Decimal(New Integer() {5, 0, 0, 65536})
        '
        'YellowLinePanel2
        '
        Me.YellowLinePanel2.BackColor = System.Drawing.Color.Red
        Me.YellowLinePanel2.ForeColor = System.Drawing.Color.DimGray
        Me.YellowLinePanel2.Location = New System.Drawing.Point(12, 33)
        Me.YellowLinePanel2.Name = "YellowLinePanel2"
        Me.YellowLinePanel2.Size = New System.Drawing.Size(2, 375)
        Me.YellowLinePanel2.TabIndex = 78
        '
        'VerticalLine2
        '
        Me.VerticalLine2.BackColor = System.Drawing.Color.Red
        Me.VerticalLine2.ForeColor = System.Drawing.Color.DimGray
        Me.VerticalLine2.Location = New System.Drawing.Point(518, 33)
        Me.VerticalLine2.Name = "VerticalLine2"
        Me.VerticalLine2.Size = New System.Drawing.Size(2, 375)
        Me.VerticalLine2.TabIndex = 79
        '
        'YellowLinePanel1
        '
        Me.YellowLinePanel1.BackColor = System.Drawing.Color.Red
        Me.YellowLinePanel1.ForeColor = System.Drawing.Color.DimGray
        Me.YellowLinePanel1.Location = New System.Drawing.Point(4, 204)
        Me.YellowLinePanel1.Name = "YellowLinePanel1"
        Me.YellowLinePanel1.Size = New System.Drawing.Size(1011, 2)
        Me.YellowLinePanel1.TabIndex = 80
        '
        'lSaveFolder
        '
        Me.lSaveFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lSaveFolder.Location = New System.Drawing.Point(781, 5)
        Me.lSaveFolder.Name = "lSaveFolder"
        Me.lSaveFolder.Size = New System.Drawing.Size(230, 21)
        Me.lSaveFolder.TabIndex = 81
        Me.lSaveFolder.Text = "Save Folder"
        '
        'bTest
        '
        Me.bTest.Enabled = False
        Me.bTest.Location = New System.Drawing.Point(1223, 544)
        Me.bTest.Name = "bTest"
        Me.bTest.Size = New System.Drawing.Size(50, 23)
        Me.bTest.TabIndex = 82
        Me.bTest.Text = "Test"
        Me.bTest.UseVisualStyleBackColor = True
        '
        'lSpectrRecording
        '
        Me.lSpectrRecording.AutoSize = True
        Me.lSpectrRecording.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lSpectrRecording.ForeColor = System.Drawing.Color.LimeGreen
        Me.lSpectrRecording.Location = New System.Drawing.Point(1111, 444)
        Me.lSpectrRecording.Name = "lSpectrRecording"
        Me.lSpectrRecording.Size = New System.Drawing.Size(108, 16)
        Me.lSpectrRecording.TabIndex = 95
        Me.lSpectrRecording.Text = "Not Recording"
        '
        'lMessage
        '
        Me.lMessage.AutoSize = True
        Me.lMessage.Location = New System.Drawing.Point(1095, 621)
        Me.lMessage.Name = "lMessage"
        Me.lMessage.Size = New System.Drawing.Size(0, 13)
        Me.lMessage.TabIndex = 94
        '
        'lSpectrCounter
        '
        Me.lSpectrCounter.AutoSize = True
        Me.lSpectrCounter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lSpectrCounter.Location = New System.Drawing.Point(1081, 616)
        Me.lSpectrCounter.Name = "lSpectrCounter"
        Me.lSpectrCounter.Size = New System.Drawing.Size(46, 15)
        Me.lSpectrCounter.TabIndex = 93
        Me.lSpectrCounter.Text = "Counter"
        '
        'cbSaveSpectrums
        '
        Me.cbSaveSpectrums.AutoSize = True
        Me.cbSaveSpectrums.Location = New System.Drawing.Point(980, 616)
        Me.cbSaveSpectrums.Name = "cbSaveSpectrums"
        Me.cbSaveSpectrums.Size = New System.Drawing.Size(104, 17)
        Me.cbSaveSpectrums.TabIndex = 92
        Me.cbSaveSpectrums.Text = "Save Spectrums"
        Me.cbSaveSpectrums.UseVisualStyleBackColor = True
        '
        'LEDconnected
        '
        Me.LEDconnected.BackColor = System.Drawing.Color.Red
        Me.LEDconnected.Location = New System.Drawing.Point(1084, 442)
        Me.LEDconnected.Name = "LEDconnected"
        Me.LEDconnected.Size = New System.Drawing.Size(21, 18)
        Me.LEDconnected.TabIndex = 91
        Me.LEDconnected.UseVisualStyleBackColor = False
        '
        'lSpectrConnected
        '
        Me.lSpectrConnected.AutoSize = True
        Me.lSpectrConnected.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lSpectrConnected.Location = New System.Drawing.Point(1065, 426)
        Me.lSpectrConnected.Name = "lSpectrConnected"
        Me.lSpectrConnected.Size = New System.Drawing.Size(92, 13)
        Me.lSpectrConnected.TabIndex = 86
        Me.lSpectrConnected.Text = "Not Connected"
        '
        'bClear
        '
        Me.bClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bClear.Location = New System.Drawing.Point(1058, 546)
        Me.bClear.Name = "bClear"
        Me.bClear.Size = New System.Drawing.Size(75, 32)
        Me.bClear.TabIndex = 90
        Me.bClear.Text = "Get Clear"
        Me.bClear.UseVisualStyleBackColor = True
        '
        'bDark
        '
        Me.bDark.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bDark.Location = New System.Drawing.Point(978, 546)
        Me.bDark.Name = "bDark"
        Me.bDark.Size = New System.Drawing.Size(75, 32)
        Me.bDark.TabIndex = 89
        Me.bDark.Text = "Get Dark"
        Me.bDark.UseVisualStyleBackColor = True
        '
        'bStopSpectrAcq
        '
        Me.bStopSpectrAcq.Location = New System.Drawing.Point(1060, 581)
        Me.bStopSpectrAcq.Name = "bStopSpectrAcq"
        Me.bStopSpectrAcq.Size = New System.Drawing.Size(73, 29)
        Me.bStopSpectrAcq.TabIndex = 85
        Me.bStopSpectrAcq.Text = "&Stop"
        Me.bStopSpectrAcq.UseVisualStyleBackColor = True
        '
        'bConnectSpectrometer
        '
        Me.bConnectSpectrometer.Location = New System.Drawing.Point(978, 426)
        Me.bConnectSpectrometer.Name = "bConnectSpectrometer"
        Me.bConnectSpectrometer.Size = New System.Drawing.Size(83, 37)
        Me.bConnectSpectrometer.TabIndex = 87
        Me.bConnectSpectrometer.Text = "Connect spectrometer"
        Me.bConnectSpectrometer.UseVisualStyleBackColor = True
        '
        'bContinuous
        '
        Me.bContinuous.Location = New System.Drawing.Point(978, 581)
        Me.bContinuous.Name = "bContinuous"
        Me.bContinuous.Size = New System.Drawing.Size(75, 29)
        Me.bContinuous.TabIndex = 84
        Me.bContinuous.Text = "Start"
        Me.bContinuous.UseVisualStyleBackColor = True
        '
        'bGetSpectrum
        '
        Me.bGetSpectrum.Location = New System.Drawing.Point(1133, 546)
        Me.bGetSpectrum.Name = "bGetSpectrum"
        Me.bGetSpectrum.Size = New System.Drawing.Size(64, 39)
        Me.bGetSpectrum.TabIndex = 83
        Me.bGetSpectrum.Text = "G&et spectrum"
        Me.bGetSpectrum.UseVisualStyleBackColor = True
        '
        'gUSB2000
        '
        Me.gUSB2000.Controls.Add(Me.LEDSpectrOpt)
        Me.gUSB2000.Controls.Add(Me.Label6)
        Me.gUSB2000.Controls.Add(Me.NumExpXsec)
        Me.gUSB2000.Controls.Add(Me.cbRestrictBand)
        Me.gUSB2000.Controls.Add(Me.cbAutoSpectr)
        Me.gUSB2000.Controls.Add(Me.lOptimal)
        Me.gUSB2000.Controls.Add(Me.Label24)
        Me.gUSB2000.Controls.Add(Me.Label23)
        Me.gUSB2000.Controls.Add(Me.NumScans)
        Me.gUSB2000.Controls.Add(Me.NumIntTime)
        Me.gUSB2000.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gUSB2000.Location = New System.Drawing.Point(976, 459)
        Me.gUSB2000.Margin = New System.Windows.Forms.Padding(0)
        Me.gUSB2000.Name = "gUSB2000"
        Me.gUSB2000.Padding = New System.Windows.Forms.Padding(0)
        Me.gUSB2000.Size = New System.Drawing.Size(209, 84)
        Me.gUSB2000.TabIndex = 88
        Me.gUSB2000.TabStop = False
        '
        'LEDSpectrOpt
        '
        Me.LEDSpectrOpt.BackColor = System.Drawing.Color.Red
        Me.LEDSpectrOpt.Location = New System.Drawing.Point(163, 34)
        Me.LEDSpectrOpt.Name = "LEDSpectrOpt"
        Me.LEDSpectrOpt.Size = New System.Drawing.Size(21, 18)
        Me.LEDSpectrOpt.TabIndex = 62
        Me.LEDSpectrOpt.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(163, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "Exp/s"
        '
        'NumExpXsec
        '
        Me.NumExpXsec.DecimalPlaces = 1
        Me.NumExpXsec.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.NumExpXsec.Location = New System.Drawing.Point(105, 58)
        Me.NumExpXsec.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.NumExpXsec.Minimum = New Decimal(New Integer() {500, 0, 0, -2147418112})
        Me.NumExpXsec.Name = "NumExpXsec"
        Me.NumExpXsec.Size = New System.Drawing.Size(52, 20)
        Me.NumExpXsec.TabIndex = 33
        Me.NumExpXsec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumExpXsec.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cbRestrictBand
        '
        Me.cbRestrictBand.AutoSize = True
        Me.cbRestrictBand.Location = New System.Drawing.Point(3, 61)
        Me.cbRestrictBand.Name = "cbRestrictBand"
        Me.cbRestrictBand.Size = New System.Drawing.Size(103, 17)
        Me.cbRestrictBand.TabIndex = 32
        Me.cbRestrictBand.Text = "Restrict Band"
        Me.cbRestrictBand.UseVisualStyleBackColor = True
        '
        'cbAutoSpectr
        '
        Me.cbAutoSpectr.AutoSize = True
        Me.cbAutoSpectr.Location = New System.Drawing.Point(105, 34)
        Me.cbAutoSpectr.Name = "cbAutoSpectr"
        Me.cbAutoSpectr.Size = New System.Drawing.Size(52, 17)
        Me.cbAutoSpectr.TabIndex = 31
        Me.cbAutoSpectr.Text = "Auto"
        Me.cbAutoSpectr.UseVisualStyleBackColor = True
        '
        'lOptimal
        '
        Me.lOptimal.AutoSize = True
        Me.lOptimal.Location = New System.Drawing.Point(153, 13)
        Me.lOptimal.Name = "lOptimal"
        Me.lOptimal.Size = New System.Drawing.Size(49, 13)
        Me.lOptimal.TabIndex = 30
        Me.lOptimal.Text = "Optimal"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(8, 34)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(44, 13)
        Me.Label24.TabIndex = 27
        Me.Label24.Text = "CoAdd"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(8, 13)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(84, 13)
        Me.Label23.TabIndex = 26
        Me.Label23.Text = "Int. Time (ms)"
        '
        'NumScans
        '
        Me.NumScans.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumScans.Location = New System.Drawing.Point(58, 32)
        Me.NumScans.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.NumScans.Name = "NumScans"
        Me.NumScans.Size = New System.Drawing.Size(42, 20)
        Me.NumScans.TabIndex = 25
        '
        'NumIntTime
        '
        Me.NumIntTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumIntTime.Location = New System.Drawing.Point(89, 11)
        Me.NumIntTime.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumIntTime.Name = "NumIntTime"
        Me.NumIntTime.Size = New System.Drawing.Size(58, 20)
        Me.NumIntTime.TabIndex = 24
        Me.NumIntTime.Value = New Decimal(New Integer() {500, 0, 0, 0})
        '
        'USB2000chart
        '
        ChartArea5.AxisX.LineColor = System.Drawing.Color.White
        ChartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.White
        ChartArea5.AxisY.LineColor = System.Drawing.Color.White
        ChartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.White
        ChartArea5.BackColor = System.Drawing.Color.Black
        ChartArea5.BorderColor = System.Drawing.Color.White
        ChartArea5.Name = "ChartArea1"
        Me.USB2000chart.ChartAreas.Add(ChartArea5)
        Legend5.Enabled = False
        Legend5.Name = "Legend1"
        Me.USB2000chart.Legends.Add(Legend5)
        Me.USB2000chart.Location = New System.Drawing.Point(4, 428)
        Me.USB2000chart.Name = "USB2000chart"
        Me.USB2000chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me.USB2000chart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Yellow}
        Series5.ChartArea = "ChartArea1"
        Series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine
        Series5.Legend = "Legend1"
        Series5.Name = "Series1"
        Me.USB2000chart.Series.Add(Series5)
        Me.USB2000chart.Size = New System.Drawing.Size(647, 213)
        Me.USB2000chart.TabIndex = 97
        Me.USB2000chart.Text = "Chart1"
        '
        'RatioChart1
        '
        ChartArea6.AxisX.IsLabelAutoFit = False
        ChartArea6.AxisX.LabelAutoFitMinFontSize = 5
        ChartArea6.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea6.AxisX.LineColor = System.Drawing.Color.White
        ChartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.White
        ChartArea6.AxisX.MaximumAutoSize = 90.0!
        ChartArea6.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea6.AxisY.LabelAutoFitMinFontSize = 5
        ChartArea6.AxisY.LineColor = System.Drawing.Color.White
        ChartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.White
        ChartArea6.BackColor = System.Drawing.Color.Black
        ChartArea6.Name = "ChartArea1"
        Me.RatioChart1.ChartAreas.Add(ChartArea6)
        Legend6.Enabled = False
        Legend6.Name = "Legend1"
        Me.RatioChart1.Legends.Add(Legend6)
        Me.RatioChart1.Location = New System.Drawing.Point(633, 421)
        Me.RatioChart1.Name = "RatioChart1"
        Me.RatioChart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me.RatioChart1.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Yellow}
        Series6.ChartArea = "ChartArea1"
        Series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine
        Series6.Legend = "Legend1"
        Series6.Name = "Series1"
        Me.RatioChart1.Series.Add(Series6)
        Me.RatioChart1.Size = New System.Drawing.Size(345, 213)
        Me.RatioChart1.TabIndex = 98
        Me.RatioChart1.Text = "RatioChart1"
        '
        'TimerSpectr
        '
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3, Me.TSSLtemperature, Me.lToolStripDebug1, Me.lToolStripDebug2, Me.ToolStripErrors})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 636)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1303, 24)
        Me.StatusStrip1.TabIndex = 99
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 19)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(43, 19)
        Me.ToolStripStatusLabel2.Text = "Status"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(4, 19)
        '
        'TSSLtemperature
        '
        Me.TSSLtemperature.Name = "TSSLtemperature"
        Me.TSSLtemperature.Size = New System.Drawing.Size(38, 19)
        Me.TSSLtemperature.Text = "Temp"
        '
        'lToolStripDebug1
        '
        Me.lToolStripDebug1.Name = "lToolStripDebug1"
        Me.lToolStripDebug1.Size = New System.Drawing.Size(48, 19)
        Me.lToolStripDebug1.Text = "Debug1"
        '
        'lToolStripDebug2
        '
        Me.lToolStripDebug2.Name = "lToolStripDebug2"
        Me.lToolStripDebug2.Size = New System.Drawing.Size(48, 19)
        Me.lToolStripDebug2.Text = "Debug2"
        '
        'ToolStripErrors
        '
        Me.ToolStripErrors.Name = "ToolStripErrors"
        Me.ToolStripErrors.Size = New System.Drawing.Size(27, 19)
        Me.ToolStripErrors.Text = "----"
        '
        'cbLongExp
        '
        Me.cbLongExp.AutoSize = True
        Me.cbLongExp.Location = New System.Drawing.Point(1088, 311)
        Me.cbLongExp.Name = "cbLongExp"
        Me.cbLongExp.Size = New System.Drawing.Size(101, 17)
        Me.cbLongExp.TabIndex = 100
        Me.cbLongExp.Text = "Long Exposition"
        Me.cbLongExp.UseVisualStyleBackColor = True
        '
        'lLongExposure
        '
        Me.lLongExposure.AutoSize = True
        Me.lLongExposure.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lLongExposure.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lLongExposure.Location = New System.Drawing.Point(1101, 331)
        Me.lLongExposure.Name = "lLongExposure"
        Me.lLongExposure.Size = New System.Drawing.Size(111, 16)
        Me.lLongExposure.TabIndex = 101
        Me.lLongExposure.Text = "Long Exposure"
        '
        'LtimerAcq
        '
        Me.LtimerAcq.AutoSize = True
        Me.LtimerAcq.Location = New System.Drawing.Point(1195, 311)
        Me.LtimerAcq.Name = "LtimerAcq"
        Me.LtimerAcq.Size = New System.Drawing.Size(98, 13)
        Me.LtimerAcq.TabIndex = 102
        Me.LtimerAcq.Text = "TmrAqNot Running"
        '
        'Ltimer1
        '
        Me.Ltimer1.AutoSize = True
        Me.Ltimer1.Location = New System.Drawing.Point(1199, 329)
        Me.Ltimer1.Name = "Ltimer1"
        Me.Ltimer1.Size = New System.Drawing.Size(92, 13)
        Me.Ltimer1.TabIndex = 103
        Me.Ltimer1.Text = "Tmr1 not Running"
        '
        'TimerDebug
        '
        Me.TimerDebug.Enabled = True
        Me.TimerDebug.Interval = 200
        '
        'lDebug4
        '
        Me.lDebug4.AutoSize = True
        Me.lDebug4.Location = New System.Drawing.Point(1168, 47)
        Me.lDebug4.Name = "lDebug4"
        Me.lDebug4.Size = New System.Drawing.Size(47, 13)
        Me.lDebug4.TabIndex = 104
        Me.lDebug4.Text = "lDebug4"
        '
        'lDebug5
        '
        Me.lDebug5.AutoSize = True
        Me.lDebug5.Location = New System.Drawing.Point(1123, 47)
        Me.lDebug5.Name = "lDebug5"
        Me.lDebug5.Size = New System.Drawing.Size(47, 13)
        Me.lDebug5.TabIndex = 105
        Me.lDebug5.Text = "lDebug5"
        '
        'fMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1303, 660)
        Me.Controls.Add(Me.lDebug5)
        Me.Controls.Add(Me.lDebug4)
        Me.Controls.Add(Me.Ltimer1)
        Me.Controls.Add(Me.LtimerAcq)
        Me.Controls.Add(Me.lLongExposure)
        Me.Controls.Add(Me.cbLongExp)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.RatioChart1)
        Me.Controls.Add(Me.USB2000chart)
        Me.Controls.Add(Me.lSpectrRecording)
        Me.Controls.Add(Me.lMessage)
        Me.Controls.Add(Me.lSpectrCounter)
        Me.Controls.Add(Me.cbSaveSpectrums)
        Me.Controls.Add(Me.LEDconnected)
        Me.Controls.Add(Me.lSpectrConnected)
        Me.Controls.Add(Me.bClear)
        Me.Controls.Add(Me.bDark)
        Me.Controls.Add(Me.bStopSpectrAcq)
        Me.Controls.Add(Me.bConnectSpectrometer)
        Me.Controls.Add(Me.bContinuous)
        Me.Controls.Add(Me.bGetSpectrum)
        Me.Controls.Add(Me.gUSB2000)
        Me.Controls.Add(Me.bTest)
        Me.Controls.Add(Me.lSaveFolder)
        Me.Controls.Add(Me.YellowLinePanel1)
        Me.Controls.Add(Me.VerticalLine2)
        Me.Controls.Add(Me.YellowLinePanel2)
        Me.Controls.Add(Me.gbSampling)
        Me.Controls.Add(Me.PanelLine)
        Me.Controls.Add(Me.LCamRecording)
        Me.Controls.Add(Me.lStatus)
        Me.Controls.Add(Me.lCounter)
        Me.Controls.Add(Me.setupButton1)
        Me.Controls.Add(Me.cbUD2)
        Me.Controls.Add(Me.cbUD1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lDebug3)
        Me.Controls.Add(Me.bAutoAcq)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lExpData)
        Me.Controls.Add(Me.cbUsePeak)
        Me.Controls.Add(Me.gbALC)
        Me.Controls.Add(Me.cbAutoExp)
        Me.Controls.Add(Me.cbSave)
        Me.Controls.Add(Me.StopButton)
        Me.Controls.Add(Me.ALCTypeComboBox)
        Me.Controls.Add(Me.bSaveSetting)
        Me.Controls.Add(Me.bStopVideo2)
        Me.Controls.Add(Me.bVideo2)
        Me.Controls.Add(Me.bStopVideo1)
        Me.Controls.Add(Me.bVideo1)
        Me.Controls.Add(Me.lDebug2)
        Me.Controls.Add(Me.ldebug)
        Me.Controls.Add(Me.bConnect)
        Me.Controls.Add(Me.lCamID2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lCamID1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bEnd)
        Me.Controls.Add(Me.gbHidden)
        Me.Name = "fMain"
        Me.Text = "JAISpectr01"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbALC.ResumeLayout(False)
        Me.gbALC.PerformLayout()
        CType(Me.setpointTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gAutoALC.ResumeLayout(False)
        Me.gAutoALC.PerformLayout()
        Me.ExposureGroupBox.ResumeLayout(False)
        Me.ExposureGroupBox.PerformLayout()
        CType(Me.ExposureTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GainGroupBox.ResumeLayout(False)
        Me.GainGroupBox.PerformLayout()
        CType(Me.GainTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbHidden.ResumeLayout(False)
        Me.groupBox5.ResumeLayout(False)
        Me.groupBox5.PerformLayout()
        CType(Me.roiheightNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.roiwidthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.yposNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xposNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBox4.ResumeLayout(False)
        Me.groupBox4.PerformLayout()
        CType(Me.maxExposureNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.maxGainNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.minExposureNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.minGainNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ImageSizeGroupBox.ResumeLayout(False)
        Me.ImageSizeGroupBox.PerformLayout()
        CType(Me.HeightNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WidthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSampling.ResumeLayout(False)
        Me.gbSampling.PerformLayout()
        CType(Me.exposureUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gUSB2000.ResumeLayout(False)
        Me.gUSB2000.PerformLayout()
        CType(Me.NumExpXsec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumScans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumIntTime, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.USB2000chart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RatioChart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bEnd As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lCamID1 As System.Windows.Forms.Label
    Friend WithEvents lCamID2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bConnect As System.Windows.Forms.Button
    Friend WithEvents ldebug As System.Windows.Forms.Label
    Friend WithEvents lDebug2 As System.Windows.Forms.Label
    Friend WithEvents bVideo1 As System.Windows.Forms.Button
    Friend WithEvents bStopVideo1 As System.Windows.Forms.Button
    Friend WithEvents bStopVideo2 As System.Windows.Forms.Button
    Friend WithEvents bVideo2 As System.Windows.Forms.Button
    Friend WithEvents bSaveSetting As System.Windows.Forms.Button
    Private WithEvents ALCTypeComboBox As System.Windows.Forms.ComboBox
    Private WithEvents StopButton As System.Windows.Forms.Button
    Private WithEvents cbSave As System.Windows.Forms.CheckBox
    Private WithEvents cbAutoExp As System.Windows.Forms.CheckBox
    Friend WithEvents gbALC As System.Windows.Forms.GroupBox
    Private WithEvents setpointTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents setpointTextBox As System.Windows.Forms.TextBox
    Friend WithEvents gAutoALC As System.Windows.Forms.GroupBox
    Private WithEvents exposureTextBox As System.Windows.Forms.TextBox
    Private WithEvents gainTextBox As System.Windows.Forms.TextBox
    Private WithEvents ExposureGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents lExposure As System.Windows.Forms.Label
    Private WithEvents ExposureTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents GainGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents GainLabel As System.Windows.Forms.Label
    Private WithEvents GainTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Private WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cbUsePeak As System.Windows.Forms.CheckBox
    Friend WithEvents lExpData As System.Windows.Forms.Label
    Friend WithEvents TimerAcq As System.Windows.Forms.Timer
    Friend WithEvents bAutoAcq As System.Windows.Forms.Button
    Friend WithEvents lDebug3 As System.Windows.Forms.Label
    Friend WithEvents gbHidden As System.Windows.Forms.GroupBox
    Private WithEvents groupBox5 As System.Windows.Forms.GroupBox
    Private WithEvents roiheightNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents roiwidthNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents yposNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents xposNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents label17 As System.Windows.Forms.Label
    Private WithEvents label16 As System.Windows.Forms.Label
    Private WithEvents label15 As System.Windows.Forms.Label
    Private WithEvents label14 As System.Windows.Forms.Label
    Private WithEvents groupBox4 As System.Windows.Forms.GroupBox
    Private WithEvents maxExposureNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents maxGainNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents minExposureNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents minGainNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents label13 As System.Windows.Forms.Label
    Private WithEvents label12 As System.Windows.Forms.Label
    Private WithEvents label11 As System.Windows.Forms.Label
    Private WithEvents label10 As System.Windows.Forms.Label
    Private WithEvents ImageSizeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents HeightNumericUpDown As System.Windows.Forms.NumericUpDown
    Private WithEvents WidthNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbUD1 As System.Windows.Forms.CheckBox
    Friend WithEvents cbUD2 As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents setupButton1 As System.Windows.Forms.Button
    Friend WithEvents lCounter As System.Windows.Forms.Label
    Friend WithEvents LCamRecording As System.Windows.Forms.Label
    Friend WithEvents lStatus As System.Windows.Forms.Label
    Friend WithEvents PanelLine As System.Windows.Forms.Panel
    Friend WithEvents gbSampling As System.Windows.Forms.GroupBox
    Friend WithEvents exposureUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents YellowLinePanel2 As System.Windows.Forms.Panel
    Friend WithEvents VerticalLine2 As System.Windows.Forms.Panel
    Friend WithEvents YellowLinePanel1 As System.Windows.Forms.Panel
    Friend WithEvents lSaveFolder As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Private WithEvents bTest As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lSpectrRecording As System.Windows.Forms.Label
    Friend WithEvents lMessage As System.Windows.Forms.Label
    Friend WithEvents lSpectrCounter As System.Windows.Forms.Label
    Friend WithEvents cbSaveSpectrums As System.Windows.Forms.CheckBox
    Friend WithEvents LEDconnected As System.Windows.Forms.Button
    Friend WithEvents lSpectrConnected As System.Windows.Forms.Label
    Friend WithEvents bClear As System.Windows.Forms.Button
    Friend WithEvents bDark As System.Windows.Forms.Button
    Friend WithEvents bStopSpectrAcq As System.Windows.Forms.Button
    Friend WithEvents bConnectSpectrometer As System.Windows.Forms.Button
    Friend WithEvents bContinuous As System.Windows.Forms.Button
    Friend WithEvents bGetSpectrum As System.Windows.Forms.Button
    Friend WithEvents gUSB2000 As System.Windows.Forms.GroupBox
    Friend WithEvents LEDSpectrOpt As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents NumExpXsec As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbRestrictBand As System.Windows.Forms.CheckBox
    Friend WithEvents cbAutoSpectr As System.Windows.Forms.CheckBox
    Friend WithEvents lOptimal As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents NumScans As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumIntTime As System.Windows.Forms.NumericUpDown
    Friend WithEvents USB2000chart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents RatioChart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents TimerSpectr As System.Windows.Forms.Timer
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSSLtemperature As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lToolStripDebug1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lToolStripDebug2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbLongExp As System.Windows.Forms.CheckBox
    Friend WithEvents lLongExposure As System.Windows.Forms.Label
    Friend WithEvents ToolStripErrors As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LtimerAcq As System.Windows.Forms.Label
    Friend WithEvents Ltimer1 As System.Windows.Forms.Label
    Friend WithEvents TimerDebug As System.Windows.Forms.Timer
    Friend WithEvents lDebug4 As System.Windows.Forms.Label
    Friend WithEvents lDebug5 As System.Windows.Forms.Label

End Class
