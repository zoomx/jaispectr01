﻿Imports Jai_FactoryDotNET
Imports System.IO  'Required by Path.GetDirectoryName
Imports System.Reflection  'Required by Assembly.GetExecutingAssembly
Imports System.Drawing

Public Class fMain
    Public Structure SpectraPoint
        'Declare data members
        Public Value As Double
        Public Index As UInteger
        'The structure is value and array index
    End Structure

    Public Structure PixelPoint
        Public X As Integer
        Public Y As Integer
    End Structure
    Public Const MaxSamples As Integer = 1023 ''1023 ''2047

    Public debugg As Boolean = True
    Public LtimerAcq_Text As String ' = "TmrAqNot Running"
    Public Ltimer1_Text As String '= "Tmr1 not Running"

    'Folders
    Private BaseFolder As String
    Private ActualFolder As String
    Private LogFolder As String
    Private SpectrumFolder As String ' base Folder where spectrums are saved
    Private DayFolder As String 'A folder were all spectrum of same day are saved
    Private SaveFolderBase As String
    Private Savefolder As String
    Private SavefolderToday As String
    Private CameraFolder As String
    Private DataDrive As String

    Private Station As String
    Private txStation_Text As String
    Private lBrightness_text As String

    'Basefolder -> DayFolder 	-> CameraFolder
    '                           -> SpectrumFolder

    Private AutomaticRun As Boolean = True 'True
    Private GetCameraBySerial As Boolean = True

    'Spectrometer
    Public ActualSpectrum(MaxSamples) As Double
    Public WaveLenght(MaxSamples) As Double
    Public Ratios(400) As Single
    Public iRatio As Integer = 0
    Public Const MaxIndex As Integer = MaxSamples
    Public Const FirstWaveLenght As Single = 309        'Wavelenght for ratio calculation
    Public iFirstWaveLenght As Integer = 0
    Public Const SecondWaveLenght As Single = 335        'Wavelenght for ratio calculation
    Public iSecondWaveLenght As Integer = 0

    Public FirstWaveLenghtOptimal As Single = 0        'Wavelenght for optimal exposition
    Public iFirstWaveLenghtOptimal As Integer = 0
    Public Const SecondWaveLenghtOptimal As Single = 340        'Wavelenght for optimal exposition
    Public iSecondWaveLenghtOptimal As Integer = 0

    Public SpectraMax As SpectraPoint
    Public SpectraMin As SpectraPoint
    Public MaxPeak As Double
    Public MaxCounts As UInteger = 65535    'Maybe can be changed for different spectrometers and read from registry or from spectrometer itself?
    Public CoeffHighOtpimalLevel As Single = 0.9 'Values from the javascript program by V.I. Tsanev (vip20@cam.ac.uk) 
    Public CoefflowOtpimalLevel As Single = 0.8  'must be 0.8
    Public HighOptimalLevel As ULong
    Public LowOptimalLevel As ULong
    Public MaxIntegrationTime As ULong = 200000

    Public wrapper As New OmniDriver.CCoWrapper 'This is the wrapper for specrometer driver

    Public Temperature As Single
    Public numberOfSpectrometers As Integer
    Public spectrometerIndex As Integer
    Public spectrometerName As String
    Public spectrometerSerial As String
    Public SpectrCounter As ULong = 0

    Public TimeStart As Date 'Used for measuring aquisition time
    Public TimeStop As Date  'Used for measuring aquisition time
    Public dAcquisitionTime As Long 'Used for measuring aquisition time
    Public dAcquisitionTimeTotal As Long 'Used for measuring aquisition time with elaborations
    Public NonlinearityCorrection As Boolean
    Public DarkElectricalCorrection As Boolean
    Public IsTemperatureSupported As Boolean
    Public CalIntercept As Double   'Calibration
    Public CalFirstCoef As Double   'Calibration
    Public CalSecondCoef As Double  'Calibration
    Public CalThirdCoef As Double   'Calibration

    Public ImageCounter As ULong = 0

    Public IntegrationTime As ULong ' = 2000
    Public ScansToAverage As UInteger

    Public Today As Date
    Public sToday As String 'Only date, non time
    Public SessionStart As String 'Time start of entire measure session
    Public DataFormat As String = "yyyy/MM/dd"
    Public TimeFormat As String = "HH:mm:ss"
    Public DateTimeFormat As String = "yyyy/MM/dd HH:mm:ss"
    Public SpectraNumber As ULong   ' Number of spectra acquired

    Public LogFile As String
    Public FileNameSpectrum As String
    Public FullLog As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) & "\JAIFullLog.txt"
    Public FullLogWriter As New System.IO.StreamWriter(FullLog, True)
    Public RatioLog As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) & "\Ratio.txt"
    Public RatioWriter As New System.IO.StreamWriter(RatioLog, True)
    Public ColorControl As System.Drawing.Color

    Public JaiCameraLog As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) & "\JAIControlTwoEmergencyLog.txt"
    Public LogWriter As IO.StreamWriter = New IO.StreamWriter(JaiCameraLog, True)


    Public myFactory As CFactory = New CFactory

    Public camName(3) As String 'Here there is the filter number, not really the cam name
    Public camSerial(3) As String
    Private camModel(3) As String

    Public Cam1SerialExpected As String = "S141318"
    Public Cam2SerialExpected As String = "S141293"
    'Coppia non fissa

    'Public Cam1SerialExpected As String = "S141322"
    'Public Cam2SerialExpected As String = "S141294"
    'Coppia fissa sull'Etna alla Montagnola

    Private Cam1Index As Integer
    Private Cam2Index As Integer

    Private CamIntensity As UInt16
    Private BestIntensity As UInt16 = 58000
    Private MaxIntensity As UInt16 = 0
    Private StartExpositionTime As Single = 0.5

    Private PixelPointControl As PixelPoint
    Private PixelMatrix(10) As PixelPoint
    Private PixelMatrixPoints As Integer
    Private PixelMatrixMaxIndex As Integer
    Private UseMatrix As Boolean = True

    Public myCamera1 As CCamera
    Public myWidthNode1 As CNode
    Public myHeightNode1 As CNode
    Public myGainNode1 As CNode
    Private myExposureNode1 As CNode
    Public MyCameraID1 As String = "Camera1"
    Public MyCameraSerial1 As String
    Public myImageInfo1 As Jai_FactoryWrapper.ImageInfo = New Jai_FactoryWrapper.ImageInfo()

    Public myCamera2 As CCamera
    Public myWidthNode2 As CNode
    Public myHeightNode2 As CNode
    Public myGainNode2 As CNode
    Private myExposureNode2 As CNode
    Public MyCameraID2 As String = "Camera2"
    Public MyCameraSerial2 As String
    Public myImageInfo2 As Jai_FactoryWrapper.ImageInfo = New Jai_FactoryWrapper.ImageInfo()

    Public myImage1ViewWindowHandle As IntPtr = IntPtr.Zero
    Public myImage2ViewWindowHandle As IntPtr = IntPtr.Zero

    Public MaxVideoExposureReached As Boolean = False
    Public MaxManualExposure As Integer = 10
    Public ManualExposureMilliseconds As Integer = 500


    Private m_ALCController As ALC = New ALC    'The Closed-loop controller
    'Private indice As Long
    ' Private Property ImageList As List(Of Jai_FactoryWrapper.ImageInfo)
    Private m_ConversionBuffer As Jai_FactoryWrapper.ImageInfo    ' Buffer used for converting images before average is calculated
    Private m_MeasureRect As Jai_FactoryWrapper.RECT
    Private m_OldRAverage As Integer   'Cached value for the last measured average in the image
    Private m_OldGAverage As Integer   'Cached value for the last measured average in the image
    Private m_OldBAverage As Integer   'Cached value for the last measured average in the image
    Private m_UpdateUI As Boolean = False   ' Flag used for signalling that the GUI needs to be updated
    Private m_bAutoExposure As Boolean = False  'Auto Exposure ON/OFF
    Private m_bUseAverage As Boolean = True 'True if Average is used, false if Peak is used
    Private m_Width As Integer = 0  'Width of image
    Private m_Height As Integer = 0  'Height of image
    Private nomefile As String = Date.Now.ToUniversalTime().ToString("yyyyMMdd_HHmmss.ff", Globalization.CultureInfo.InvariantCulture)

    Private IsVideo As Boolean = False
    Dim connected As Boolean = False
    Private VideoFromCam As Integer

    Public Running As Boolean
    Public LongExposure As Boolean
    Public UseLongExposure As Boolean = False

    'Multipoint
    Private ControlPoints(20) As Jai_FactoryWrapper.POINT
    Dim iControlPoints As Integer = 0
    Dim ControlPointsIndex As Integer = 0
    Public MaxIntensityy As Integer
    Const MaxIntensityBound As Integer = 1000
    Const MinIntensityBound As Integer = 800
    Public AverageImage As Integer
    Public maxx As Integer 'Histogram max
    Public minx As Integer 'Histogram mid
    Public Midpoint As Integer = 10000 / 64 'midpoint =65535-Midpoint

    Public MaxHistogram As Integer = 0
    Public MaxHistogramLimit As Integer = 10
    Public MaxHistogramIndex As Integer

    Public TempoEsposizione As Integer = 1000
    Public MinIntensity As Integer = 25000 / 64

    'Yellow horizontal line move
    Private Go As Boolean
    Private LeftSet As Boolean
    Private TopSet As Boolean
    ' These will hold the mouse position
    Private HoldLeft As Integer
    Private HoldTop As Integer
    ' These will hold the offset of the mouse in the element
    Private OffLeft As Integer
    Private OffTop As Integer

    'Yellow vertical line move
    Private Gov As Boolean
    Private LeftSetv As Boolean
    Private TopSetv As Boolean
    ' These will hold the mouse position
    Private HoldLeftv As Integer
    Private HoldTopv As Integer
    ' These will hold the offset of the mouse in the element
    Private OffLeftv As Integer
    Private OffTopv As Integer
    Private LeftVerticalLineLimit As Integer
    Private RightVerticalLineLimit As Integer
    Private NewLeft As Integer
    Private SecondVerticalLineShift As Integer

    Public State As String 'Cam be Running or Stopped
    'To modify header print see FileContentGenerator()
    Public Pan As Integer = 0
    Public Tilt As Integer = 0

    Public Messaggio As String

    Private Sub fMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim errore As Jai_FactoryWrapper.EFactoryError = Jai_FactoryWrapper.EFactoryError.Success

        WriteLogWithTimestamp("Starting")

        camName(1) = "310"
        camName(2) = "330"
        Station = "Montagnola"
        txStation_Text = "Montagnola"

        WriteLogWithTimestamp("Get settings")
        GetSetting()


        'Get settings from register
        'GetSetting()

        'Define Base Folder
        'SaveFolderBase = "D:\webcam"
        BaseFolder = SaveFolderBase
        If BaseFolder = "" Then
            BaseFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal)
        End If
        lSaveFolder.Text = BaseFolder
        Savefolder = BaseFolder

        GenerateFoldersNames()

        ' Open the factory with the default Registry database
        WriteLogWithTimestamp("Opening the JAI factory")
        errore = myFactory.Open("")
        WriteLogWithTimestamp(errore.ToString)

        ' Search for cameras and update all controls
        WriteLogWithTimestamp("Searching cameras")
        SearchButton_Click(Nothing, Nothing)

        WriteLogWithTimestamp("Defining Multipoint Matrix")
        DefineMatrixMultipoint()

        'indice = 0
        ALCTypeComboBox.SelectedIndex = 0 'Minimum Gain priority
        GainTrackBar.Value = 0

        WriteLogWithTimestamp("Drawing alignment lines")
        SecondVerticalLineShift = PictureBox2.Left + YellowLinePanel2.Left - PictureBox1.Left
        VerticalLine2.Left = SecondVerticalLineShift

        Running = False
        'Reading exposition parameters from INI or from Registry or defined here or from spectrometer itself
        HighOptimalLevel = CULng(MaxCounts * CoeffHighOtpimalLevel)
        LowOptimalLevel = CULng(MaxCounts * CoefflowOtpimalLevel)

        lToolStripDebug1.Text = "Levels =" & HighOptimalLevel.ToString & " " & LowOptimalLevel.ToString

        DisableButtons()
        gUSB2000.Enabled = False

        WriteLogWithTimestamp("MainForm_load end")
        'AutomaticRun = False
        If AutomaticRun = True Then
            WriteLogWithTimestamp("Starting automating acquisition")
            DoAutomaticAcquisition()
        End If
    End Sub

    Private Sub fMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        WriteLogWithTimestamp("MainForm closing, stopping cameras")
        If (Not (myCamera1) Is Nothing) Then
            myCamera1.StopAcquisition()
            myCamera1.Close()
        End If
        If (Not (myCamera2) Is Nothing) Then
            myCamera2.StopAcquisition()
            myCamera2.Close()
        End If
        WriteLogWithTimestamp("MainForm closing done")
    End Sub

    Private Sub bEnd_Click(sender As System.Object, e As System.EventArgs) Handles bEnd.Click
        WriteLogWithTimestamp("End pressed, stopping cameras")
        If (Not (myCamera1) Is Nothing) Then
            myCamera1.StopAcquisition()
            myCamera1.Close()
        End If
        If (Not (myCamera2) Is Nothing) Then
            myCamera2.StopAcquisition()
            myCamera2.Close()
        End If
        WriteLogWithTimestamp("End pressed, done")
        End
    End Sub

    Public Sub SearchButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Search for any new cameras using Filter Driver
        myFactory.UpdateCameraList(Jai_FactoryDotNET.CFactory.EDriverType.FilterDriver)
        If (myFactory.CameraList.Count >= 2) Then
            ' Open the camera

            If GetCameraBySerial = True Then
                Dim i As Integer

                'Search for first camera
                For i = 0 To myFactory.CameraList.Count
                    myCamera1 = myFactory.CameraList(i)
                    If myCamera1.SerialNumber = Cam1SerialExpected Then
                        Cam1Index = i
                        WriteLogWithTimestamp("Found camera 1 with serial " & Cam1SerialExpected & " on index " & Cam1Index.ToString)
                        Exit For
                    Else
                        myCamera1 = Nothing
                    End If
                Next

                If myCamera1 Is Nothing Then
                    MsgBox("Camera 1 with serial " & Cam1SerialExpected & " not found!")
                End If

                'Search for second camera
                For i = 0 To myFactory.CameraList.Count
                    myCamera2 = myFactory.CameraList(i)
                    If myCamera2.SerialNumber = Cam2SerialExpected Then
                        Cam2Index = i
                        WriteLogWithTimestamp("Found camera 2 with serial " & Cam2SerialExpected & " on index " & Cam2Index.ToString)
                        Exit For
                    Else
                        myCamera2 = Nothing
                    End If
                Next

                If myCamera2 Is Nothing Then
                    MsgBox("Camera 2 with serial " & Cam2SerialExpected & " not found!")
                End If
            Else

                myCamera1 = myFactory.CameraList(0)
                myCamera2 = myFactory.CameraList(1)
                MyCameraID1 = myFactory.CameraList(0).CameraID
                MyCameraID2 = myFactory.CameraList(1).CameraID
            End If



            ' MyCameraID1 = myCamera1.CameraID.ToString   'Restituisce una stringa troppo lunga

            'lCamID1.Text = MyCameraID1
            lCamID1.Text = myCamera1.SerialNumber
            lCamID2.Text = myCamera2.SerialNumber

            If (Jai_FactoryWrapper.EFactoryError.Success = myCamera1.Open) Then
            Else
                WriteLogWithTimestamp("Error opening cam1")
                If AutomaticRun = False Then
                    MsgBox("Error opening cam1")
                    Exit Sub
                Else
                    End
                End If
            End If

            If (Jai_FactoryWrapper.EFactoryError.Success = myCamera2.Open) Then
            Else
                WriteLogWithTimestamp("Error opening cam2")
                If AutomaticRun = False Then
                    MsgBox("Error opening cam2")
                    Exit Sub
                Else
                    End
                End If
            End If

            WriteLogWithTimestamp("Opening cameras")

            myCamera1.Open()
            'myCamera1.reset 'ma non esite questo comando, purtroppo
            myCamera2.Open()

            WriteLogWithTimestamp("Getting parameters from cameras")

            m_ALCController.SetKGain(25)
            m_ALCController.SetKExposure(3)

            bConnect.Enabled = True
            StopButton.Enabled = True
            Dim currentValue As Integer = 0

            WriteLogWithTimestamp("Getting Width")
            ' Get the Width GenICam Node
            myWidthNode2 = myCamera2.GetNode("Width")
            If (Not (myWidthNode2) Is Nothing) Then
                currentValue = Integer.Parse(myWidthNode2.Value.ToString)
                m_Width = Integer.Parse(myWidthNode2.Value.ToString())
                ' Update range for the Numeric Up/Down control
                ' Convert from integer to Decimal type
                WidthNumericUpDown.Maximum = Decimal.Parse(myWidthNode2.Max.ToString)
                WidthNumericUpDown.Minimum = Decimal.Parse(myWidthNode2.Min.ToString)
                WidthNumericUpDown.Value = Decimal.Parse(currentValue.ToString)
                WidthNumericUpDown.Enabled = True
            Else
                WidthNumericUpDown.Enabled = False
            End If

            WriteLogWithTimestamp("Getting Height")
            ' Get the Height GenICam Node
            myHeightNode2 = myCamera2.GetNode("Height")
            If (Not (myHeightNode2) Is Nothing) Then
                currentValue = Integer.Parse(myHeightNode2.Value.ToString)
                m_Height = Integer.Parse(myHeightNode2.Value.ToString())
                ' Update range for the Numeric Up/Down control
                ' Convert from integer to Decimal type
                HeightNumericUpDown.Maximum = Decimal.Parse(myHeightNode2.Max.ToString)
                HeightNumericUpDown.Minimum = Decimal.Parse(myHeightNode2.Min.ToString)
                HeightNumericUpDown.Value = Decimal.Parse(currentValue.ToString)
                HeightNumericUpDown.Enabled = True
            Else
                HeightNumericUpDown.Enabled = False
            End If

            WriteLogWithTimestamp("Getting Gain parameters")
            ' Get the GainRaw GenICam Node
            myGainNode1 = myCamera1.GetNode("GainRaw")
            myGainNode2 = myCamera2.GetNode("GainRaw")

            If (Not (myGainNode2) Is Nothing) Then
                ' Update the values inside the ALC controller
                m_ALCController.SetMinGain(Convert.ToInt32(myGainNode2.Min))
                m_ALCController.SetMaxGain(Convert.ToInt32(myGainNode2.Max))
                m_ALCController.SetCurrentGain(Convert.ToInt32(myGainNode2.Value))

                currentValue = Integer.Parse(myGainNode2.Value.ToString)
                ' Update range for the TrackBar Controls
                GainTrackBar.Maximum = Integer.Parse(myGainNode2.Max.ToString)
                GainTrackBar.Minimum = Integer.Parse(myGainNode2.Min.ToString)
                GainTrackBar.Value = currentValue
                GainTrackBar.TickFrequency = (GainTrackBar.Maximum - GainTrackBar.Minimum) / 20
                GainLabel.Text = myGainNode2.Value.ToString
                minGainNumericUpDown.Minimum = Convert.ToInt32(myGainNode2.Min)
                minGainNumericUpDown.Maximum = Convert.ToInt32(myGainNode2.Max)
                minGainNumericUpDown.Value = Convert.ToInt32(myGainNode2.Min)
                maxGainNumericUpDown.Minimum = Convert.ToInt32(myGainNode2.Min)
                maxGainNumericUpDown.Maximum = Convert.ToInt32(myGainNode2.Max)
                maxGainNumericUpDown.Value = Convert.ToInt32(myGainNode2.Max)

                'GainTrackBar.Maximum = Integer.Parse(myGainNode2.Max.ToString)         'Ma è ripetuto? Perchè?
                GainLabel.Text = myGainNode2.Value.ToString
                GainLabel.Enabled = True
                GainTrackBar.Enabled = True
            Else
                GainLabel.Enabled = False
                GainTrackBar.Enabled = False
            End If

            WriteLogWithTimestamp("Getting Exposure parameters")
            ' Get the Exposition GenICam Node
            'myExposureNode = myCamera.GetNode("ExposureTimeRaw")
            myExposureNode1 = myCamera1.GetNode("ExposureTime")
            myExposureNode2 = myCamera2.GetNode("ExposureTime")
            If (Not (myExposureNode2) Is Nothing) Then

                'Update the values inside the ALC controller
                m_ALCController.SetMinExposure(Convert.ToInt32(myExposureNode2.Min))
                m_ALCController.SetMaxExposure(Convert.ToInt32(myExposureNode2.Max))
                m_ALCController.SetCurrentExposure(Convert.ToInt32(myExposureNode2.Value))

                'ExposureTrackBar.Minimum = Convert.ToInt32(myExposureNode2.Min)         'Ma è ripetuto? Perchè?
                'ExposureTrackBar.Maximum = Convert.ToInt32(myExposureNode2.Max)         'Ma è ripetuto? Perchè?
                'ExposureTrackBar.Value = Convert.ToInt32(myExposureNode2.Value)         'Ma è ripetuto? Perchè?
                'ExposureTrackBar.TickFrequency = (ExposureTrackBar.Maximum - ExposureTrackBar.Minimum) / 20         'Ma è ripetuto? Perchè?
                exposureTextBox.Text = Convert.ToInt32(myExposureNode2.Value).ToString
                minExposureNumericUpDown.Minimum = Convert.ToInt32(myExposureNode2.Min)
                minExposureNumericUpDown.Maximum = Convert.ToInt32(myExposureNode2.Max)
                minExposureNumericUpDown.Value = Convert.ToInt32(myExposureNode2.Min)
                maxExposureNumericUpDown.Minimum = Convert.ToInt32(myExposureNode2.Min)
                maxExposureNumericUpDown.Maximum = Convert.ToInt32(myExposureNode2.Max)
                maxExposureNumericUpDown.Value = Convert.ToInt32(myExposureNode2.Max)


                'currentValue = Integer.Parse(myExposureNode.Value.ToString)
                currentValue = Double.Parse(myExposureNode2.Value.ToString)
                ' Update range for the TrackBar Controls
                ExposureTrackBar.Maximum = Integer.Parse(myExposureNode2.Max.ToString)
                ExposureTrackBar.Minimum = Integer.Parse(myExposureNode2.Min.ToString)
                ExposureTrackBar.Value = currentValue
                ExposureTrackBar.TickFrequency = ((ExposureTrackBar.Maximum - ExposureTrackBar.Minimum) / 10)
                lExposure.Text = Convert.ToInt32(myExposureNode2.Value).ToString
                lExposure.Enabled = True
                ExposureTrackBar.Enabled = True
            Else
                GainLabel.Enabled = False
                GainTrackBar.Enabled = False
            End If

            WriteLogWithTimestamp("Setting cameras to 10bit")
            'Set pixel format to 10bit
            myCamera1.GetNode("PixelFormat").Value = "Mono10"
            myCamera2.GetNode("PixelFormat").Value = "Mono10"

            WriteLogWithTimestamp("Getting PixelFormat parameters")
            Dim pixelformatValue As Object = myCamera2.GetNodeValue("PixelFormat")
            'pixelformatValue=
            Dim enumValue As CNode.IEnumValue = CType(pixelformatValue, CNode.IEnumValue)
            Dim BytesPerPixel As UInteger = Jai_FactoryWrapper.GetPixelTypeMemorySize(CType(enumValue.Value, Jai_FactoryWrapper.EPixelFormatType))
            If (BytesPerPixel = 1) Then
                m_ALCController.SetSetPoint(128)
                setpointTrackBar.Minimum = 0
                setpointTrackBar.Maximum = 255
                setpointTrackBar.Value = m_ALCController.GetSetPoint
                setpointTextBox.Text = setpointTrackBar.Value.ToString
                setpointTrackBar.TickFrequency = (255 / 20)
            Else
                m_ALCController.SetSetPoint(32768)
                setpointTrackBar.Minimum = 0
                setpointTrackBar.Maximum = 65535
                setpointTrackBar.Value = m_ALCController.GetSetPoint
                setpointTextBox.Text = setpointTrackBar.Value.ToString
                setpointTrackBar.TickFrequency = (65535 / 20)
            End If

            WriteLogWithTimestamp("Setting parameters for average exposure")
            ' Set up average measurement ROI as centre 1/3 of the image area as default
            m_MeasureRect.Left = (m_Width / 3)
            m_MeasureRect.Top = (m_Width / 3)   '?????? m_Width and not m_Height????
            m_MeasureRect.Right = (m_Width * (2 / 3))
            m_MeasureRect.Bottom = (m_Height * (2 / 3))

            ' Set up average measurement ROI as whole image
            m_MeasureRect.Left = 0
            m_MeasureRect.Top = 0
            m_MeasureRect.Right = m_Width - 1
            m_MeasureRect.Bottom = m_Height - 1


            roiwidthNumericUpDown.Minimum = 0
            roiwidthNumericUpDown.Maximum = m_Width
            roiwidthNumericUpDown.Value = (m_MeasureRect.Right - m_MeasureRect.Left)
            roiheightNumericUpDown.Minimum = 0
            roiheightNumericUpDown.Maximum = m_Height
            roiheightNumericUpDown.Value = (m_MeasureRect.Bottom - m_MeasureRect.Top)
            xposNumericUpDown.Minimum = 0
            xposNumericUpDown.Maximum = m_Width
            xposNumericUpDown.Value = m_MeasureRect.Left
            yposNumericUpDown.Minimum = 0
            yposNumericUpDown.Maximum = m_Width
            yposNumericUpDown.Value = m_MeasureRect.Top
        Else
            If myFactory.CameraList.Count = 0 Then
                MessageBox.Show("No Cameras Found!")
                bConnect.Enabled = False
                StopButton.Enabled = False
                'stretchCheckBox.Enabled = False
                WidthNumericUpDown.Enabled = False
                HeightNumericUpDown.Enabled = False
                GainLabel.Enabled = False
                GainTrackBar.Enabled = False
            Else
                myCamera1 = myFactory.CameraList(0)
                WriteLogWithTimestamp("Found only camera with serial " & myCamera1.SerialNumber)
                MessageBox.Show("Found only one camera! " & myCamera1.SerialNumber)
                myCamera1 = Nothing
            End If
        End If
        WriteLogWithTimestamp("Search finished")
    End Sub

    Private Sub bConnect_Click(sender As System.Object, e As System.EventArgs) Handles bConnect.Click
        bConnect.Enabled = False

        If connected = False Then
            'If the camera is disconnected
            WriteLogWithTimestamp("Connecting cameras")
            IsVideo = False
            StartExposition()
            Application.DoEvents()
            bConnect.Text = "Disconnect"
            EnableCamButtons()
            connected = True
            WriteLogWithTimestamp("Cameras connected")
        Else
            If (Not (myCamera1) Is Nothing) Then
                WriteLogWithTimestamp("Disconnecting camera 1")
                myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera1.GetNode("TriggerMode").Value = "Off"
                Application.DoEvents()
                myCamera1.GetNode("ExposureMode").Value = "Continuous"
                Application.DoEvents()
                myCamera1.StopImageAcquisition()
                Application.DoEvents()
                RemoveHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
                Application.DoEvents()
                'StartButton.Enabled = True
                'StopButton.Enabled = False
                'SWTriggerButton.Enabled = False
                WriteLogWithTimestamp("Camera 1 disconnected")
            End If
            If (Not (myCamera2) Is Nothing) Then
                WriteLogWithTimestamp("disconnecting camera 2")
                myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera2.GetNode("TriggerMode").Value = "Off"
                Application.DoEvents()
                myCamera2.GetNode("ExposureMode").Value = "Continuous"
                Application.DoEvents()
                myCamera2.StopImageAcquisition()
                Application.DoEvents()
                RemoveHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
                Application.DoEvents()
                'StartButton.Enabled = True
                'StopButton.Enabled = False
                'SWTriggerButton.Enabled = False
                WriteLogWithTimestamp("Camera 2 disconnected")
            End If
            bConnect.Text = "Connect"
            DisableCamButtons()
            connected = False
            WriteLogWithTimestamp("End of disconnection")
        End If

        bConnect.Enabled = True
    End Sub

    Private Sub StartExposition()

        'It is not a start of acquisition
        'Cameras are connected and video is diplayed in the PictureBoxes

        WriteLogWithTimestamp("StartExposition")

        If (Not (myCamera1) Is Nothing) Then
            Try
                myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera1.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
                myCamera2.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
                myCamera1.GetNode("TriggerSource").Value = "Software"
                Application.DoEvents()
                myCamera2.GetNode("TriggerSource").Value = "Software"
                Application.DoEvents()

                AddHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
                Application.DoEvents()
                AddHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
                Application.DoEvents()
                myCamera1.StartImageAcquisition(True, 5, PictureBox1.Handle)
                Application.DoEvents()
                myCamera2.StartImageAcquisition(True, 5, PictureBox2.Handle)
                Application.DoEvents()
            Catch
                WriteEmergencyLogWithTimestamp("Unhandled Exception on opening cam")
            End Try
        End If
        'Many differences with ConnectCameras in JAIControlTwo

        'ldebug.Text = "Stop acquisition"


    End Sub
    ' Local callback function used for handle new images
    Private Sub HandleImage1(ByRef ImageInfo As Jai_FactoryWrapper.ImageInfo)
        'Dim FileName As String

        Dim errore As Jai_FactoryWrapper.EFactoryError = Jai_FactoryWrapper.EFactoryError.Success
        Dim Average As Jai_FactoryWrapper.PixelValue = New Jai_FactoryWrapper.PixelValue
        Dim Average2 As Jai_FactoryWrapper.PixelValue = New Jai_FactoryWrapper.PixelValue

        Average2.MONO10_Y = 0

        Average.BGR48_R = 0
        Average.BGR48_G = 0
        Average.BGR48_B = 0

        'HISTOGRAM
        Dim MyHistogram As Jai_FactoryWrapper.HistogramInfo

        'indice = (indice + 1)
        'If cbSave.Checked = True Then
        '    Dim filename As String = GenerateName(1)
        '    'nomefile = Date.Now.ToUniversalTime().ToString("yyyyMMdd_HHmmss.ff", Globalization.CultureInfo.InvariantCulture) & "_310.tif"
        '    'FileName = String.Format("image_{0}.tiff", indice)
        '    Jai_FactoryWrapper.J_Image_SaveFile(ImageInfo, filename)
        '    ' lDebug3.Text = "saved"
        '    ImageCounter = ImageCounter + 1

        'End If

        If cbAutoExp.Checked = True And IsVideo = True Then
            ' We need to allocate conversion buffer once
            If (m_ConversionBuffer.ImageBuffer = IntPtr.Zero) Then
                errore = Jai_FactoryWrapper.J_Image_Malloc(ImageInfo, m_ConversionBuffer)
            End If

            ' Then we convert into 48bpp image format
            errore = Jai_FactoryWrapper.J_Image_FromRawToImage(ImageInfo, m_ConversionBuffer, Jai_FactoryWrapper.EColorInterpolationAlgorithm.BayerStandardMultiprocessor, 4096, 4096, 4096)


            ' .. and then we can calculate the average value for the measurement ROI
            errore = Jai_FactoryWrapper.J_Image_GetAverage(m_ConversionBuffer, m_MeasureRect, Average)
            errore = Jai_FactoryWrapper.J_Image_GetAverage(ImageInfo, m_MeasureRect, Average2)

            'HISTOGRAM
            If IsVideo Then
                Dim MaxEntries As Integer
                errore = Jai_FactoryWrapper.J_Image_MallocHistogram(ImageInfo, MyHistogram)
                errore = Jai_FactoryWrapper.J_Image_CreateHistogram(ImageInfo, m_MeasureRect, MyHistogram)
                MaxEntries = MyHistogram.HistEntries - 1
                errore = Jai_FactoryWrapper.J_Image_GetHistogramValue(MyHistogram, 0, MaxEntries, maxx)
                errore = Jai_FactoryWrapper.J_Image_FreeHistogram(MyHistogram)


                Dim averagei As Integer = Average.BGR48_G
                Dim averagei2 As Integer = Average2.MONO10_Y

                If (CType(Average.BGR48_R, Integer) <> m_OldRAverage) Then
                    m_OldRAverage = CType(Average.BGR48_R, Integer)
                    m_UpdateUI = True
                End If

                Dim peak As Integer = averagei
                AverageImage = averagei
                Dim changes As Boolean = False

                If m_bUseAverage Then
                    changes = m_ALCController.Calculate(averagei)
                    'changes = m_ALCController.Calculate(averagei2)
                Else
                    Dim index As Integer
                    Dim intensity As Integer = 0
                    Dim MaxIntensity As Integer = 0
                    Dim PixelValue As Jai_FactoryWrapper.PixelValue

                    For index = 0 To iControlPoints
                        errore = Jai_FactoryWrapper.J_Image_GetPixel(ImageInfo, ControlPoints(index), PixelValue)
                        intensity = PixelValue.MONO10_Y
                        If MaxIntensity < intensity Then
                            MaxIntensity = intensity
                            ControlPointsIndex = index  'Maybe useful for statistical purpouse, wich point is usually the max
                        End If
                    Next
                    'WriteLogWithTimestamp("MaxIntensity" & MaxIntensity.ToString)
                    MaxIntensityy = MaxIntensity
                    'm_ALCController.SetSetPoint(1000)
                    changes = m_ALCController.Calculate(MaxIntensity * 64)
                    'changes = m_ALCController.Calculate(peak)
                End If
                If changes Then
                    ' Send the new values to the camera
                    UpdateGainAndExposureValuesForVideo(1)
                    ' UpdateGainAndExposureValues()
                    m_UpdateUI = True
                End If
            End If
        End If


    End Sub
    ' Local callback function used for handle new images
    Private Sub HandleImage2(ByRef ImageInfo As Jai_FactoryWrapper.ImageInfo)
        'Dim FileName As String

        Dim errore As Jai_FactoryWrapper.EFactoryError = Jai_FactoryWrapper.EFactoryError.Success
        Dim Average As Jai_FactoryWrapper.PixelValue = New Jai_FactoryWrapper.PixelValue
        Dim Average2 As Jai_FactoryWrapper.PixelValue = New Jai_FactoryWrapper.PixelValue
        Average2.MONO10_Y = 0

        Average.BGR48_R = 0
        Average.BGR48_G = 0
        Average.BGR48_B = 0

        'HISTOGRAM
        Dim MyHistogram As Jai_FactoryWrapper.HistogramInfo

        If LongExposure = True Then
            WriteLogWithTimestamp("LongExposure-Showing Image")
        End If

        If cbAutoExp.Checked = True Then

            ' We need to allocate conversion buffer once
            If (m_ConversionBuffer.ImageBuffer = IntPtr.Zero) Then
                errore = Jai_FactoryWrapper.J_Image_Malloc(ImageInfo, m_ConversionBuffer)
            End If

            ' Then we convert into 48bpp image format
            errore = Jai_FactoryWrapper.J_Image_FromRawToImage(ImageInfo, m_ConversionBuffer, Jai_FactoryWrapper.EColorInterpolationAlgorithm.BayerStandardMultiprocessor, 4096, 4096, 4096)


            ' .. and then we can calculate the average value for the measurement ROI
            errore = Jai_FactoryWrapper.J_Image_GetAverage(m_ConversionBuffer, m_MeasureRect, Average)
            errore = Jai_FactoryWrapper.J_Image_GetAverage(ImageInfo, m_MeasureRect, Average2)

            'HISTOGRAM
            Dim MaxEntries As Integer
            errore = Jai_FactoryWrapper.J_Image_MallocHistogram(ImageInfo, MyHistogram)
            errore = Jai_FactoryWrapper.J_Image_CreateHistogram(ImageInfo, m_MeasureRect, MyHistogram)
            MaxEntries = MyHistogram.HistEntries - 1
            errore = Jai_FactoryWrapper.J_Image_GetHistogramValue(MyHistogram, 0, MaxEntries, maxx)
            MaxEntries = MyHistogram.HistEntries - Midpoint
            errore = Jai_FactoryWrapper.J_Image_GetHistogramValue(MyHistogram, 0, MaxEntries, minx)
            'Find MaxHistogram
            Dim i As Integer
            'Dim hindex As Integer
            For i = 1023 To 0 Step -1
                errore = Jai_FactoryWrapper.J_Image_GetHistogramValue(MyHistogram, 0, i, MaxHistogram)
                If MaxHistogram > MaxHistogramLimit Then
                    MaxHistogramIndex = i
                    Exit For
                End If
            Next

            errore = Jai_FactoryWrapper.J_Image_FreeHistogram(MyHistogram)


            Dim averagei As Integer = Average.BGR48_G
            Dim averagei2 As Integer = Average2.MONO10_Y

            If (CType(Average.BGR48_R, Integer) <> m_OldRAverage) Then
                m_OldRAverage = CType(Average.BGR48_R, Integer)
                m_UpdateUI = True
            End If
            If (CType(Average.BGR48_G, Integer) <> m_OldGAverage) Then
                m_OldGAverage = CType(Average.BGR48_G, Integer)
                m_UpdateUI = True
            End If
            If (CType(Average.BGR48_B, Integer) <> m_OldBAverage) Then
                m_OldBAverage = CType(Average.BGR48_B, Integer)
                m_UpdateUI = True
            End If


            Dim peak As Integer = averagei
            AverageImage = averagei
            Dim changes As Boolean = False
            If m_bUseAverage Then
                changes = m_ALCController.Calculate(averagei)
                'changes = m_ALCController.Calculate(averagei2)
            Else
                Dim index As Integer
                Dim intensity As Integer = 0
                Dim MaxIntensity As Integer = 0
                Dim PixelValue As Jai_FactoryWrapper.PixelValue

                For index = 0 To iControlPoints
                    errore = Jai_FactoryWrapper.J_Image_GetPixel(ImageInfo, ControlPoints(index), PixelValue)
                    intensity = PixelValue.MONO10_Y
                    If MaxIntensity < intensity Then
                        MaxIntensity = intensity
                        ControlPointsIndex = index  'Maybe useful for statistical purpouse, wich point is usually the max
                    End If
                Next
                'WriteLogWithTimestamp("MaxIntensity" & MaxIntensity.ToString)


                'Qui devo decidere se usare l'algoritmo automatico o passare alla esposizione lunga manuale



                'changes = m_ALCController.Calculate(peak)
            End If

            MaxIntensity = MaxHistogram
            'MaxIntensity = maxx / 64
            MaxIntensityy = MaxIntensity


            'If changes Then
            '    ' Send the new values to the camera
            '    If IsVideo = True Then
            '        UpdateGainAndExposureValuesForVideo(2)
            '    Else
            '        UpdateGainAndExposureValues()
            '    End If
            '    m_UpdateUI = True
            'End If

            'If MaxIntensity = 0 Then MaxIntensity = BestIntensity

            If MaxIntensity < MinIntensity And myGainNode2.Value = Convert.ToInt32(myGainNode2.Max) And cbLongExp.Checked = True Then
                'Se l'intensità massima è sotto una soglia e l'esposizione è già al massimo
                'passiamo all'esposizione lunga
                TimerAcq.Enabled = False
                LtimerAcq_Text = "TmrAcq Not running0"
                If LongExposure = False Then
                    WriteLogWithTimestamp("Using Longexposure")
                End If
                LongExposure = True
                lLongExposure.ForeColor = Color.GreenYellow

                'calcolo esposizione
                If MaxIntensity > 60000 / 64 Then ' If b > 0.7 Then
                    'If CamIntensity > 64500 Then
                    TempoEsposizione = TempoEsposizione * 0.8
                    WriteLogWithTimestamp("TempoEsposizione * 0.8")
                End If
                If MaxIntensity < 50000 / 64 Then 'If b < 0.55 Then
                    If MaxIntensity < 10000 / 64 Then
                        TempoEsposizione = TempoEsposizione * 1.2
                        WriteLogWithTimestamp("TempoEsposizione * 1.2")
                    Else
                        TempoEsposizione = TempoEsposizione * BestIntensity / TempoEsposizione
                        WriteLogWithTimestamp("TempoEsposizione * BestIntensity / TempoEsposizione")
                    End If
                End If
                'exposureUpDown.Value = TempoEsposizione
                WriteLogWithTimestamp("Exposure = " & TempoEsposizione)


                'lAutoExpState.Text = "Optimizing"
                'LEDCamOpt.BackColor = Color.Red
                If TempoEsposizione > 10000 Then
                    TempoEsposizione = 10000
                    WriteLogWithTimestamp("Exposition too long!!")
                    'If AutomaticRun = True Then cbSave.Checked = False
                Else
                    'If AutomaticRun = True Then cbSave.Checked = True
                End If
                Messaggio = "Exposure Time ->" + Str(TempoEsposizione)
                WriteLogWithTimestamp(Messaggio)
                ManualExposure()
                'Forse in esposizione manuale manca la parte del calcolo dell'esposizione?
            Else
                If LongExposure = True Then
                    WriteLogWithTimestamp("Using normal exposure")
                    ReConnectCameras()
                    'If AutomaticRun = True Then cbSave.Checked = True
                    'TimerAcq.Enabled = True
                    'Timer1.Enabled = True
                    'LongExposure = False
                End If

                'm_bUseAverage = False

                changes = m_ALCController.Calculate(MaxHistogramIndex * 64) 'ALC works in the range 0-65535 instead maxintensity is 0-1023
                'If maxx > 1000 Then
                '    changes = m_ALCController.Calculate(65534) 'ALC works in the range 0-65535 instead maxintensity and histogram is 0-1023
                'End If
                'If minx = 0 Then
                '    changes = m_ALCController.Calculate(65535 - Midpoint * 64) 'ALC works in the range 0-65535 instead maxintensity is 0-1023
                'End If
                If changes Then
                    ' Send the new values to the camera
                    If IsVideo = True Then
                        UpdateGainAndExposureValuesForVideo(2)
                    Else
                        UpdateGainAndExposureValues()
                    End If
                    m_UpdateUI = True
                End If
                LongExposure = False
                TimerAcq.Enabled = True
                Timer1.Enabled = True
                'myCamera1.SerialNumber
                'myCamera2()
                lLongExposure.ForeColor = Color.WhiteSmoke


        End If

        End If


    End Sub

    Private Sub UpdateGainAndExposureValues()
        If (Not (myGainNode2) Is Nothing) Then
            myGainNode2.Value = m_ALCController.GetCurrentGain
        End If
        If (Not (myExposureNode2) Is Nothing) Then
            myExposureNode2.Value = m_ALCController.GetCurrentExposure
        End If
        If (Not (myGainNode1) Is Nothing) Then
            myGainNode1.Value = m_ALCController.GetCurrentGain
        End If
        If (Not (myExposureNode1) Is Nothing) Then
            myExposureNode1.Value = m_ALCController.GetCurrentExposure
        End If
    End Sub

    Private Sub UpdateGainAndExposureValuesForVideo(cam As Integer)
        If cam = 1 Then
            If (Not (myGainNode1) Is Nothing) Then
                myGainNode1.Value = m_ALCController.GetCurrentGain
            End If
            If (Not (myExposureNode1) Is Nothing) Then
                myExposureNode1.Value = m_ALCController.GetCurrentExposure
            End If
        Else
            If (Not (myGainNode2) Is Nothing) Then
                myGainNode2.Value = m_ALCController.GetCurrentGain
            End If
            If (Not (myExposureNode2) Is Nothing) Then
                myExposureNode2.Value = m_ALCController.GetCurrentExposure
            End If
        End If
    End Sub

    Private Sub bVideo1_Click(sender As System.Object, e As System.EventArgs) Handles bVideo1.Click
        AddHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
        myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
        myCamera1.GetNode("TriggerMode").Value = "Off"
        'myCamera1.GetNode("ExposureMode").Value = "Continuous trigger"
        myCamera1.GetNode("ExposureMode").Value = "Continuous"
        myCamera1.StartImageAcquisition(True, 5)
        Timer1.Enabled = True
        IsVideo = True
        VideoFromCam = 1
    End Sub

    Private Sub bStopVideo1_Click(sender As System.Object, e As System.EventArgs) Handles bStopVideo1.Click
        Timer1.Enabled = False
        Ltimer1_Text = "Tmr1 not Running"
        myCamera1.StopImageAcquisition()
        RemoveHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
        IsVideo = False
    End Sub

    Private Sub bVideo2_Click(sender As System.Object, e As System.EventArgs) Handles bVideo2.Click
        AddHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
        myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
        myCamera2.GetNode("TriggerMode").Value = "Off"
        'myCamera2.GetNode("ExposureMode").Value = "Continuous trigger"
        myCamera2.GetNode("ExposureMode").Value = "Continuous"
        myCamera2.StartImageAcquisition(True, 5)
        Timer1.Enabled = True
        IsVideo = True
        VideoFromCam = 2
    End Sub

    Private Sub bStopVideo2_Click(sender As System.Object, e As System.EventArgs) Handles bStopVideo2.Click
        RemoveHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
        Timer1.Enabled = False
        Ltimer1_Text = "Tmr1 not Running"
        myCamera2.StopImageAcquisition()
        IsVideo = False
    End Sub

    Private Sub bSaveSetting_Click(sender As System.Object, e As System.EventArgs) Handles bSaveSetting.Click
        'http://msdn.microsoft.com/en-us/library/8bf3xkta(v=vs.100).aspx
        Dim regVersion = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
                  "SOFTWARE\\INGV\\UVJAISpectr", True)
        If regVersion Is Nothing Then
            ' Key doesn't exist; create it.
            regVersion = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                         "SOFTWARE\\INGV\\UVJAISpectr")
        End If

        If camSerial(1) = "" Then
            'Nothing To save
            Exit Sub
        End If

        If regVersion IsNot Nothing Then
            regVersion.SetValue("Serial1", camSerial(1))
            regVersion.SetValue("Serial2", camSerial(2))
            regVersion.SetValue("Filter1", camName(1))
            regVersion.SetValue("Filter2", camName(2))
            'regVersion.SetValue("SaveFolderBase", SaveFolderBase)
            regVersion.SetValue("SaveFolderBase", Savefolder)
            'regVersion.SetValue("UpsideDown1", cbUD1.Checked)
            'regVersion.SetValue("UpsideDown2", cbUD2.Checked)
            regVersion.SetValue("StationName", txStation_Text)
            'regVersion.SetValue("CCDtemp", ccdTempUpDown1.Value)
            'regVersion.SetValue("CoolerOn", coolerOnCheckBox1.Checked)
            'regVersion.SetValue("Slave", cbSlave.Checked)
            'regVersion.SetValue("ReadOutSpeed", cbFastDownload.Checked)
            'regVersion.SetValue("ElectronicShutter", cbElecShutter.Checked)
            'regVersion.SetValue("PixelPointControlX", PixelPointControl.X)
            'regVersion.SetValue("PixelPointControlY", PixelPointControl.Y)
            'regVersion.SetValue("RestrictBand", cbRestrictBand.Checked)
            regVersion.Close()
        End If
        WriteLogWithTimestamp("Settings saved")
    End Sub

    Private Sub GetSetting()
        Dim regVersion = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
                  "SOFTWARE\\INGV\\UVJAISpectr", True)
        If regVersion Is Nothing Then
            WriteLogWithTimestamp("Unable to get settings maybe it is the first run")
            Exit Sub
        End If

        If regVersion IsNot Nothing Then
            camSerial(1) = CStr(regVersion.GetValue("Serial1", 0))
            camSerial(2) = CStr(regVersion.GetValue("Serial2", 0))

            If camSerial(1) <> "0" Then
                Cam1SerialExpected = camSerial(1)
            End If

            If camSerial(2) <> "0" Then
                Cam2SerialExpected = camSerial(2)
            End If

            'lCamera1.Text = camSerial(1)
            'lCamera2.Text = camSerial(2)

            'Vede se il seriale c'è ed eventualmente seleziona!!!
            'If cam1.QSIDeviceCount = 2 Then
            '    cam1.QSISelectedDevice = camSerial(1)

            '    cam2.QSISelectedDevice = camSerial(2)

            'End If

            camName(1) = CStr(regVersion.GetValue("Filter1", 0))
            If camName(1) = "0" Then
                camName(1) = "310"
            End If
            'txFilter1.Text = camName(1)
            camName(2) = CStr(regVersion.GetValue("Filter2", 0))
            If camName(2) = "0" Then
                camName(2) = "330"
            End If
            'txFilter2.Text = camName(2)

            SaveFolderBase = CStr(regVersion.GetValue("SaveFolderBase", 0))
            If SaveFolderBase = "" Then
                'Qui bisogna leggere da un file INI oppure dal registro
                SaveFolderBase = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal)
            End If
            Savefolder = SaveFolderBase
            'cbUD1.Checked = CBool(regVersion.GetValue("UpsideDown1", 0))
            'cbUD2.Checked = CBool(regVersion.GetValue("UpsideDown2", 0))

            txStation_Text = CStr(regVersion.GetValue("StationName", 0))
            If txStation_Text = "0" Then txStation_Text = "STR"
            'ccdTempUpDown1.Value = CDec(regVersion.GetValue("CCDtemp", 0))

            'coolerOnCheckBox1.Checked = regVersion.GetValue("CoolerOn", 0)

            'cbSlave.Checked = CBool(regVersion.GetValue("Slave", 0))
            'cbFastDownload.Checked = CBool(regVersion.GetValue("ReadOutSpeed", 0))

            'cbElecShutter.Checked = CBool(regVersion.GetValue("ElectronicShutter", 0))


            'Now spectrometer
            'BaseFolder = CStr(regVersion.GetValue("BaseFolder", 0))
            'If BaseFolder = "" Then
            '    'Qui bisogna leggere da un file INI oppure dal registro
            '    BaseFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal)
            'End If
            'PixelPointControl.X = regVersion.GetValue("PixelPointControlX", 0)
            'PixelPointControl.Y = regVersion.GetValue("PixelPointControlY", 0)
            'cbRestrictBand.Checked = CBool(regVersion.GetValue("RestrictBand", 0))
            regVersion.Close()
            WriteLogWithTimestamp("Getsettings done")
        End If

    End Sub

    Public Sub WriteLogWithTimestamp(stringa As String)
        stringa = Now.ToUniversalTime().ToString & " " & stringa
        Try
            FullLogWriter.Write(stringa & vbCrLf)
            FullLogWriter.Flush()
            ToolStripErrors.Text = ""
        Catch
            ToolStripErrors.Text = "Error writing on fulllog.txt"
            ToolStripErrors.ForeColor = Color.Red
            'MsgBox("Error writing on fulllog.txt")
        End Try
    End Sub

    Public Sub WriteEmergencyLogWithTimestamp(stringa As String)
        stringa = Now.ToUniversalTime().ToString & " " & stringa
        LogWriter.Write(stringa & vbCrLf)
        LogWriter.Flush()
    End Sub

    Private Sub DisableButtons()
        'bContinuous.Enabled = False
        'bGetSpectrum.Enabled = False
        'bDark.Enabled = False
        'bClear.Enabled = False
        ''bShowCoefficients.Enabled = False
        'Button1.Enabled = False
    End Sub

    Private Sub EnableButtons()
        'bContinuous.Enabled = True
        'bGetSpectrum.Enabled = True
        'bDark.Enabled = True
        'bClear.Enabled = True
        ''bShowCoefficients.Enabled = True
        'Button1.Enabled = True
    End Sub

    Private Sub DisableCamButtons()
        setupButton1.Enabled = True
        'setupButton2.Enabled = True
        bConnect.Enabled = True
        bAutoAcq.Enabled = False
        'bTest2.Enabled = False
        'bAlign.Enabled = False
        'bDark.Enabled = False

    End Sub

    Private Sub EnableCamButtons()
        setupButton1.Enabled = False
        'setupButton2.Enabled = False
        bConnect.Enabled = False 'was connect
        bAutoAcq.Enabled = True
        'bTest2.Enabled = True
        'bAlign.Enabled = True
        'bDark.Enabled = True
    End Sub


    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Ltimer1_Text = "Tmr1 Running"
        If m_UpdateUI Then

            Dim newGain As Integer = m_ALCController.GetCurrentGain()

            GainTrackBar.Value = newGain
            gainTextBox.Text = newGain.ToString()
            GainLabel.Text = GainTrackBar.Value.ToString

            Dim newExposureTime As Integer = m_ALCController.GetCurrentExposure()

            ExposureTrackBar.Value = newExposureTime
            exposureTextBox.Text = newExposureTime.ToString()
            lExposure.Text = ExposureTrackBar.Value.ToString

            lExpData.Text = (MaxIntensityy * 64).ToString & " - " & AverageImage.ToString & " - " & maxx.ToString
            m_UpdateUI = False

        End If
        lCounter.Text = ImageCounter.ToString
    End Sub

    Private Sub GainTrackBar_Scroll(sender As System.Object, e As System.EventArgs) Handles GainTrackBar.Scroll
        If IsVideo Then
            If VideoFromCam = 1 Then
                If (Not (myGainNode1) Is Nothing) Then
                    myGainNode1.Value = Integer.Parse(GainTrackBar.Value.ToString)
                End If
            Else
                If (Not (myGainNode2) Is Nothing) Then
                    myGainNode2.Value = Integer.Parse(GainTrackBar.Value.ToString)
                End If
            End If
        Else
            If (Not (myGainNode1) Is Nothing) Then
                myGainNode1.Value = Integer.Parse(GainTrackBar.Value.ToString)
            End If
            If (Not (myGainNode2) Is Nothing) Then
                myGainNode2.Value = Integer.Parse(GainTrackBar.Value.ToString)
            End If
        End If


        'GainLabel.Text = myGainNode.Value.ToString
        GainLabel.Text = GainTrackBar.Value.ToString
    End Sub

    Private Sub ExposureTrackBar_Scroll(sender As System.Object, e As System.EventArgs) Handles ExposureTrackBar.Scroll
        If IsVideo Then
            If VideoFromCam = 1 Then
                If (Not (myExposureNode1) Is Nothing) Then
                    myExposureNode1.Value = Integer.Parse(ExposureTrackBar.Value.ToString)
                End If
            Else
                If (Not (myExposureNode2) Is Nothing) Then
                    myExposureNode2.Value = Integer.Parse(ExposureTrackBar.Value.ToString)
                End If
            End If
        Else
            If (Not (myExposureNode1) Is Nothing) Then
                myExposureNode1.Value = Integer.Parse(ExposureTrackBar.Value.ToString)
            End If
            If (Not (myExposureNode2) Is Nothing) Then
                myExposureNode2.Value = Integer.Parse(ExposureTrackBar.Value.ToString)
            End If
        End If


        'lExposure.Text = myExposureNode.Value.ToString
        lExposure.Text = ExposureTrackBar.Value.ToString
    End Sub

    Private Sub setpointTrackBar_Scroll(sender As System.Object, e As System.EventArgs) Handles setpointTrackBar.Scroll
        m_ALCController.SetSetPoint(setpointTrackBar.Value)
        setpointTextBox.Text = setpointTrackBar.Value.ToString()
    End Sub

    Private Sub cbAutoExp_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbAutoExp.CheckedChanged
        m_bAutoExposure = cbAutoExp.Checked
        m_ALCController.SetALCType(ALC.EALCType.MinGainPriority)
        WriteLogWithTimestamp("Autoexposure " & m_bAutoExposure.ToString)
    End Sub

    Private Sub ALCTypeComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ALCTypeComboBox.SelectedIndexChanged
        'm_ALCController.SetALCType((ALC.EALCType) ALCTypeComboBox.SelectedIndex)
        m_ALCController.SetALCType(CType(ALCTypeComboBox.SelectedIndex, ALC.EALCType))
    End Sub

    Private Sub GenerateFoldersNames()
        Dim Done As Boolean
        SavefolderToday = DateAndTime.Year(Now.ToUniversalTime()) & Format(DateAndTime.Month(Now.ToUniversalTime()), "00") & Format(DateAndTime.Day(Now.ToUniversalTime()), "00")
        'No problem with midnight since we don't take images during night. Maybe in polar regions or using UTC....
        'In any case it will be the name of start acquisition
        If BaseFolder.EndsWith("\") = False Then
            BaseFolder = BaseFolder + "\"
        End If
        Done = CheckPath(BaseFolder)
        If Done = False Then
            WriteEmergencyLogWithTimestamp("Error creating Base folder!")
        End If
        DayFolder = BaseFolder + SavefolderToday + "\"
        Done = CheckPath(DayFolder)
        If Done = False Then
            WriteEmergencyLogWithTimestamp("Error creating Day folder!")
            Me.BackColor = Color.Red
            'lToolStripDebug2.Text = "Error creating folders!!!!"
            'lToolStripDebug1.Text = "Error creating folders!!!!"
            'ToolStripStatusLabel1.Text = "Error creating folders!!!!"
            'ToolStripStatusLabel2.Text = "Error creating folders!!!!"
            'ToolStripStatusLabel3.Text = "Error creating folders!!!!"
            'LCamRecording.Text = "Error!!!"
            'lSpectrRecording.Text = "Error!!!"
        End If
        'QSICameraLog = DayFolder & "\UVcamSpectr2.txt"
        FullLogWriter.Close()
        FullLog = DayFolder & "\FullLog.txt"
        FullLogWriter = New System.IO.StreamWriter(FullLog, True)
        CameraFolder = DayFolder + "images\"
        CheckPath(CameraFolder)
        SpectrumFolder = DayFolder + "spectra\"
        CheckPath(SpectrumFolder)
        DataDrive = Path.GetPathRoot(DayFolder)
        WriteLogWithTimestamp("****************************************************************************************")
        WriteLogWithTimestamp("Using " & DataDrive & " with " & My.Computer.FileSystem.GetDriveInfo(DataDrive).TotalFreeSpace.ToString & " bytes free")
    End Sub

    Private Function CheckPath(path As String) As Boolean
        'http://stackoverflow.com/questions/85996/how-do-i-create-a-folder-in-vb-if-it-doesnt-exist
        CheckPath = True
        If (Not System.IO.Directory.Exists(path)) Then
            Try
                System.IO.Directory.CreateDirectory(path)
            Catch [error] As Exception
                WriteEmergencyLogWithTimestamp("Error on creating folders")
                WriteEmergencyLogWithTimestamp([error].ToString)
                CheckPath = False
                Exit Function
            End Try
        End If
    End Function

    Private Function GenerateName(CameraNumber As Integer) As String
        GenerateName = ""
        Dim TimeNow As String
        'Dim name(2) As String
        Dim name As String
        Dim Serial As String

        Serial = camSerial(CameraNumber)
        If Serial = "" Then
            If CameraNumber = 1 Then
                Serial = "111"
            Else
                Serial = "222"
            End If
        End If

        name = camName(CameraNumber)
        If name = "" Then name = CameraNumber.ToString

        'camName(1) = txFilter1.Text
        'camName(2) = txFilter2.Text
        Station = txStation_Text
        TimeNow = Format(Year(Now.ToUniversalTime()), "0000") & Format(Month(Now.ToUniversalTime()), "00") & Format(DateAndTime.Day(Now.ToUniversalTime()), "00") & "_" & Format(Hour(Now.ToUniversalTime()), "00") & Format(Minute(Now.ToUniversalTime()), "00") & Format(Second(Now.ToUniversalTime()), "00") & "." & Format(Now.ToUniversalTime().Millisecond, "000")
        'GenerateName = Savefolder & "\" & SavefolderToday & "\" & TimeNow & "_" & Station & "_" & name
        GenerateName = CameraFolder & TimeNow & "_" & Station & "_" & name & ".tif"
        'lNames.Text = GenerateName
    End Function

    Private Sub DefineMatrixMultipoint()
        'Set control points for multipoint autoexposure
        '1392*1040
        'First center!
        ControlPoints(0).X = 696 '1392 / 2 nella QSI era 800 0.87
        ControlPoints(0).Y = 520 '1040 / 2 nella QSI era 600 0.87

        ControlPoints(1).X = 696
        ControlPoints(1).Y = 348
        ControlPoints(2).X = 696
        ControlPoints(2).Y = 174
        ControlPoints(3).X = 696
        ControlPoints(3).Y = 87
        ControlPoints(4).X = 522
        ControlPoints(4).Y = 348
        ControlPoints(5).X = 870
        ControlPoints(5).Y = 348
        ControlPoints(6).X = 435
        ControlPoints(6).Y = 174
        ControlPoints(7).X = 957
        ControlPoints(7).Y = 174
        ControlPoints(8).X = 374
        ControlPoints(8).Y = 87
        ControlPoints(9).X = 1018
        ControlPoints(9).Y = 87
        ControlPoints(10).X = 696
        ControlPoints(10).Y = 44

        ControlPoints(11).X = 10
        ControlPoints(11).Y = 10

        ControlPoints(12).X = 1380
        ControlPoints(12).Y = 10

        iControlPoints = 12
    End Sub

    Private Sub cbUsePeak_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbUsePeak.CheckedChanged
        m_bUseAverage = Not cbUsePeak.Checked
        m_ALCController.SetSetPoint(setpointTrackBar.Value)
        WriteLogWithTimestamp("UsePeak " & cbUsePeak.Checked.ToString)
    End Sub

    Private Sub TimerAcq_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAcq.Tick
        LtimerAcq_Text = "TmrAcq Running"
        'If (Not (myCamera1.GetNode("TriggerSoftware")) Is Nothing) Then
        If debugg = True Then WriteLogWithTimestamp("TimerAcq")
        If (Not (myCamera1) Is Nothing) Then
            myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
            myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
            myCamera1.GetNode("TriggerSoftware").ExecuteCommand()
            myCamera2.GetNode("TriggerSoftware").ExecuteCommand()
            myCamera1.GetNode("SoftwareTrigger0").Value = 0
            myCamera2.GetNode("SoftwareTrigger0").Value = 0
            myCamera1.GetNode("SoftwareTrigger0").Value = 1
            myCamera2.GetNode("SoftwareTrigger0").Value = 1
            myCamera1.GetNode("SoftwareTrigger0").Value = 0
            myCamera2.GetNode("SoftwareTrigger0").Value = 0
            'lDebug3.Text = "Shot"
            'WriteLogWithTimestamp("Shot")

            If cbSave.Checked = True Then
                Dim Immagine As Jai_FactoryDotNET.Jai_FactoryWrapper.ImageInfo
                Dim filename As String = GenerateName(1)
                Immagine = myCamera1.LastFrameCopy
                Jai_FactoryWrapper.J_Image_SaveFile(Immagine, filename)
                filename = GenerateName(2)
                Immagine = myCamera2.LastFrameCopy
                Jai_FactoryWrapper.J_Image_SaveFile(Immagine, filename)
                ImageCounter = ImageCounter + 1
                'WriteLogWithTimestamp("Save")
            End If

        End If

    End Sub

    Private Sub bAutoAcq_Click(sender As System.Object, e As System.EventArgs) Handles bAutoAcq.Click
        Running = True
        lStatus.Text = "Run"
        lStatus.ForeColor = Color.Red
        bAutoAcq.BackColor = Color.Red
        bAutoAcq.Enabled = False
        StopButton.Enabled = True

        WriteLogWithTimestamp("bAutoAcq - Starting camera acquisition")

        myCamera1.GetNode("AcquisitionMode").Value = "SingleFrame"
        myCamera2.GetNode("AcquisitionMode").Value = "SingleFrame"


        Timer1.Enabled = True
        TimerAcq.Enabled = True

    End Sub

    Private Sub StopButton_Click(sender As System.Object, e As System.EventArgs) Handles StopButton.Click
        Timer1.Enabled = False
        TimerAcq.Enabled = False
        LtimerAcq.Text = "Not running"
        Ltimer1_Text = "Not Running"
        bAutoAcq.Enabled = True
        lStatus.Text = "Stop"
        lStatus.ForeColor = Color.Green
        WriteLogWithTimestamp("Stopping camera acquisition")

        'Probabilmente questo potrebbe diventare un disconnect

        If (Not (myCamera1) Is Nothing) Then
            myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
            Application.DoEvents()
            myCamera1.GetNode("TriggerMode").Value = "Off"
            Application.DoEvents()
            myCamera1.GetNode("ExposureMode").Value = "Continuous"
            Application.DoEvents()
            myCamera1.StopImageAcquisition()
            Application.DoEvents()
            RemoveHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
            Application.DoEvents()
            'StartButton.Enabled = True
            'StopButton.Enabled = False
            'SWTriggerButton.Enabled = False
        End If
        If (Not (myCamera2) Is Nothing) Then
            myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
            Application.DoEvents()
            myCamera2.GetNode("TriggerMode").Value = "Off"
            Application.DoEvents()
            myCamera2.GetNode("ExposureMode").Value = "Continuous"
            Application.DoEvents()
            myCamera2.StopImageAcquisition()
            Application.DoEvents()
            RemoveHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
            Application.DoEvents()
            'StartButton.Enabled = True
            'StopButton.Enabled = False
            'SWTriggerButton.Enabled = False
        End If
        bAutoAcq.BackColor = ColorControl
        bAutoAcq.Enabled = False
        StopButton.Enabled = False
        bConnect.Enabled = True
    End Sub

    Private Sub cbSave_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbSave.CheckedChanged
        If cbSave.Checked = True Then
            LCamRecording.Text = "Recording"
            LCamRecording.ForeColor = Color.Red
            WriteLogWithTimestamp("Starting cams recording!")
        Else
            LCamRecording.Text = "Not Recording"
            LCamRecording.ForeColor = Color.LimeGreen
            WriteLogWithTimestamp("Stopping cams recording!")
            WriteLogWithTimestamp("Taken " & lCounter.Text & " images")
        End If
    End Sub

    Private Sub setupButton1_Click(sender As System.Object, e As System.EventArgs) Handles setupButton1.Click
        Me.Hide()
        fSetup.Show()
        'Open new form with a list of cameras
        'get first camera
        'Ask wich camera is
        'Get second camera and ask confirmation that is the second camera
    End Sub

 
    Private Sub YellowLinePanel1_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel1.MouseDown
        'I started with a yellow line but changed to red upon request
        Go = True
    End Sub

    Private Sub YellowLinePanel1_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel1.MouseMove
        'http://www.dreamincode.net/forums/topic/17537-click-and-drag-elements/
        'I started with a yellow line but changed to red upon request
        ' Check if the mouse is down
        If Go = True Then

            ' Set the mouse position
            HoldLeft = (Control.MousePosition.X - Me.Left)
            HoldTop = (Control.MousePosition.Y - Me.Top)

            ' Find where the mouse was clicked ONE TIME
            If TopSet = False Then
                OffTop = HoldTop - sender.Top
                ' Once the position is held, flip the switch
                ' so that it doesn't keep trying to find the position
                TopSet = True
            End If
            If LeftSet = False Then
                OffLeft = HoldLeft - sender.Left
                ' Once the position is held, flip the switch
                ' so that it doesn't keep trying to find the position
                LeftSet = True
            End If

            ' Set the position of the object
            'sender.Left = HoldLeft - OffLeft
            sender.Top = HoldTop - OffTop
        End If
    End Sub

    Private Sub YellowLinePanel1_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel1.MouseUp
        'I started with a yellow line but changed to red upon request
        Go = False
        LeftSet = False
        TopSet = False
    End Sub

    Private Sub YellowLinePanel2_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel2.MouseUp
        'I started with a yellow line but changed to red upon request
        Gov = False
        LeftSetv = False
        TopSetv = False
    End Sub

    Private Sub YellowLinePanel2_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel2.MouseDown
        'I started with a yellow line but changed to red upon request
        Gov = True
    End Sub

    Private Sub YellowLinePanel2_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles YellowLinePanel2.MouseMove
        'http://www.dreamincode.net/forums/topic/17537-click-and-drag-elements/
        'I started with a yellow line but changed to red upon request
        ' Check if the mouse is down
        If Gov = True Then

            ' Set the mouse position
            HoldLeftv = (Control.MousePosition.X - Me.Left)
            HoldTopv = (Control.MousePosition.Y - Me.Top)

            LeftVerticalLineLimit = PictureBox1.Location.X + PictureBox1.Width - YellowLinePanel2.Width - 1
            RightVerticalLineLimit = PictureBox1.Location.X - YellowLinePanel2.Width + 1
            'If HoldLeftv > LeftVerticalLineLimit Then
            '    HoldLeftv = LeftVerticalLineLimit
            'End If

            ' Find where the mouse was clicked ONE TIME
            If TopSetv = False Then
                OffTopv = HoldTopv - sender.Top
                ' Once the position is held, flip the switch
                ' so that it doesn't keep trying to find the position
                TopSetv = True
            End If
            If LeftSetv = False Then
                OffLeftv = HoldLeftv - sender.Left
                ' Once the position is held, flip the switch
                ' so that it doesn't keep trying to find the position
                LeftSetv = True
            End If

            NewLeft = HoldLeftv - OffLeftv

            ''sender.Top = HoldTopv - OffTopv
            If NewLeft > LeftVerticalLineLimit Then
                NewLeft = LeftVerticalLineLimit
            End If

            If NewLeft < RightVerticalLineLimit Then
                NewLeft = RightVerticalLineLimit
            End If

            ' Set the position of the object
            sender.Left = NewLeft
            SecondVerticalLineShift = PictureBox2.Left + YellowLinePanel2.Left - PictureBox1.Left
            VerticalLine2.Left = SecondVerticalLineShift
        End If
    End Sub

    Private Sub exposureUpDown_ValueChanged(sender As System.Object, e As System.EventArgs) Handles exposureUpDown.ValueChanged

        'ATTENTION!!!!! when activated I get an exception during start!
        'It happens because this sub is called before the control i

        Dim Interval As Integer
        Dim WasEnabled As Boolean
        WasEnabled = TimerAcq.Enabled
        TimerAcq.Enabled = False
        'LtimerAcq.Text = "Not running"
        ' LtimerAcq_Text = "Not running"
        Interval = TimerAcq.Interval
        WriteLogWithTimestamp("Old acquisition interval " & Interval.ToString)
        Interval = exposureUpDown.Value
        If Interval = 0 Then Exit Sub 'This prevents exception!!!!!
        Interval = Interval * 1000
        TimerAcq.Interval = Interval
        WriteLogWithTimestamp("New acquisition interval " & Interval.ToString)
        If WasEnabled = True Then
            TimerAcq.Enabled = True
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub lSaveFolder_Click(sender As System.Object, e As System.EventArgs) Handles lSaveFolder.Click
        'FolderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyDocuments 'restituisce un Int e qui evidentemente ci vuole un Int
        FolderBrowserDialog1.SelectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal) 'Qui invece vuole uno String per cui c'è il GetFolderPath

        FolderBrowserDialog1.ShowDialog()
        lSaveFolder.Text = FolderBrowserDialog1.SelectedPath
        Savefolder = FolderBrowserDialog1.SelectedPath
        BaseFolder = Savefolder
        GenerateFoldersNames()
        'Savefolder=
    End Sub

    Private Sub bTest_Click(sender As System.Object, e As System.EventArgs) Handles bTest.Click
        fSetup.Show()
    End Sub
    Public Sub ManualExposure()
        'If (Not (myCamera1.GetNode("TriggerSoftware")) Is Nothing) Then
        If (Not (myCamera1) Is Nothing) Then
            myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
            myCamera1.GetNode("TriggerMode").Value = "On"
            myCamera1.GetNode("TriggerSource").Value = "Software"
            myCamera1.GetNode("ExposureMode").Value = "Trigger Width"

            myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
            myCamera2.GetNode("TriggerMode").Value = "On"
            myCamera2.GetNode("TriggerSource").Value = "Software"
            myCamera2.GetNode("ExposureMode").Value = "Trigger Width"

            myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
            myCamera2.GetNode("TriggerSelector").Value = "FrameStart"

            myCamera1.GetNode("TriggerSoftware").ExecuteCommand()
            myCamera2.GetNode("TriggerSoftware").ExecuteCommand()

            myCamera1.StartImageAcquisition(True, 5)
            myCamera2.StartImageAcquisition(True, 5)

            myCamera1.GetNode("SoftwareTrigger0").Value = 0
            myCamera2.GetNode("SoftwareTrigger0").Value = 0

            myCamera1.GetNode("SoftwareTrigger0").Value = 1
            myCamera2.GetNode("SoftwareTrigger0").Value = 1

            'Label3.Text = "Iniz"
            'Label3.Text = "Iniz"
            Application.DoEvents()

            WriteLogWithTimestamp("ManualExposure - Start LongExposure acquisition")
            Application.DoEvents()
            Threading.Thread.Sleep(TempoEsposizione)
            Application.DoEvents()
            WriteLogWithTimestamp("ManualExposure - Stop LongExposure acquisition")
            Application.DoEvents()
            'Label3.Text = "Fine"
            myCamera1.GetNode("SoftwareTrigger0").Value = 0
            myCamera2.GetNode("SoftwareTrigger0").Value = 0

            'lDebug3.Text = "Shot"


            If cbSave.Checked = True Then
                Dim Immagine As Jai_FactoryDotNET.Jai_FactoryWrapper.ImageInfo
                Dim filename As String = GenerateName(1)
                Immagine = myCamera1.LastFrameCopy
                Jai_FactoryWrapper.J_Image_SaveFile(Immagine, filename)
                filename = GenerateName(2)
                Immagine = myCamera2.LastFrameCopy
                Jai_FactoryWrapper.J_Image_SaveFile(Immagine, filename)
                ImageCounter = ImageCounter + 1
            End If

        End If
    End Sub
    Private Sub ReConnectCameras()

        'It is not a start of acquisition
        'Cameras are connected and video is diplayed in the PictureBoxes

        WriteLogWithTimestamp("Executing ReConnect")

        If (Not (myCamera1) Is Nothing) Then
            Try
                myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera1.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
                myCamera2.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
                myCamera1.GetNode("TriggerSource").Value = "Software"
                Application.DoEvents()
                myCamera2.GetNode("TriggerSource").Value = "Software"
                Application.DoEvents()

                myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
                Application.DoEvents()
                myCamera1.GetNode("TriggerMode").Value = "Off"
                Application.DoEvents()
                myCamera2.GetNode("TriggerMode").Value = "Off"
                Application.DoEvents()
                'myCamera1.GetNode("ExposureMode").Value = "Continuous"
                myCamera1.GetNode("AcquisitionMode").Value = "Continuous"
                Application.DoEvents()
                'myCamera2.GetNode("ExposureMode").Value = "Continuous"
                myCamera2.GetNode("AcquisitionMode").Value = "Continuous"
                Application.DoEvents()

                myCamera1.GetNode("JAIExposureMode").Value = "Continuous trigger"
                Application.DoEvents()
                myCamera2.GetNode("JAIExposureMode").Value = "Continuous trigger"
                Application.DoEvents()


                myCamera1.GetNode("ExposureMode").Value = "Timed"
                Application.DoEvents()
                myCamera2.GetNode("ExposureMode").Value = "Timed"
                Application.DoEvents()
                'myCamera1.GetNode("MaNonCeControllo?").Value = "Continuous"

                myCamera1.GetNode("ExposureMode").Value = "Timed"
                Application.DoEvents()
                myCamera2.GetNode("ExposureMode").Value = "Timed"
                Application.DoEvents()

                'AddHandler myCamera1.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage1)
                Application.DoEvents()
                'AddHandler myCamera2.NewImageDelegate, New Jai_FactoryWrapper.ImageCallBack(AddressOf HandleImage2)
                Application.DoEvents()

                myCamera2.StartImageAcquisition(True, 5, PictureBox2.Handle)   'If you use 1 intead of 5 you get only a frame and not a stream
                Application.DoEvents()                                          'But image is not recorded!!!!!

                myCamera1.StartImageAcquisition(True, 5, PictureBox1.Handle)
                Application.DoEvents()

                'La seconda rimane scura e non avvia il flusso!
                'Anche scambiando gli avvii è sempre la seconda che rimane scura.

                myCamera1.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
                myCamera2.GetNode("TriggerSoftware").ExecuteCommand()
                Application.DoEvents()
            Catch
                MsgBox("Unhandled Exception on reconnecting cam")
            End Try

        End If


        'ldebug.Text = "Stop acquisition"


    End Sub
    '
    'From here starts spectrometers functions
    '
    '
    '
    '
    '
    Private Sub TimerSpectr_Tick(sender As System.Object, e As System.EventArgs) Handles TimerSpectr.Tick
        Dim stringa As String
        TimerSpectr.Enabled = False

        'Get spectrum
        TimeStart = Now
        Try
            ActualSpectrum = wrapper.getSpectrum(spectrometerIndex)
        Catch [error] As Exception
            WriteLogWithTimestamp("Error taking spectra (Timer1_Tick)")
            WriteLogWithTimestamp([error].ToString)
            Exit Sub
        End Try
        TimeStop = Now

        'Plot spectrum
        Try
            USB2000chart.Series(0).Points.DataBindXY(WaveLenght, ActualSpectrum)
        Catch [error] As Exception
            WriteLogWithTimestamp("Error binding spectrum to graph (Timer1_Tick)")
            WriteLogWithTimestamp([error].ToString)
            Exit Sub
        End Try

        dAcquisitionTime = CLng(TimeStop.Subtract(TimeStart).TotalMilliseconds)
        'lAcquisitionTime.Text = dAcquisitionTime.ToString
        GetTemperature(spectrometerIndex)
        GetMinMax()

        'Calculate ratio
        Try
            Ratios(399) = CSng(ActualSpectrum(iFirstWaveLenght) / ActualSpectrum(iSecondWaveLenght))
        Catch [error] As Exception
            WriteLogWithTimestamp("Error calculating ratio (Timer1_Tick) " & ActualSpectrum(iFirstWaveLenght).ToString & " " & ActualSpectrum(iSecondWaveLenght).ToString)
            WriteLogWithTimestamp([error].ToString)
            Ratios(399) = 0
            'Exit Sub
        End Try

        'Write ratio on ratiolog.txt
        stringa = Now.ToUniversalTime().ToString & " ; " & Ratios(399).ToString
        RatioWriter.Write(stringa & vbCrLf)
        RatioWriter.Flush()

        ShiftRatios()
        'RatioChart1.Update()

        Try
            RatioChart1.Series(0).Points.DataBindY(Ratios)
        Catch [error] As Exception
            WriteLogWithTimestamp("Error binding ratios to graph (Timer1_Tick)")
            WriteLogWithTimestamp([error].ToString)
            Exit Sub
        End Try

        'Calculate autoexposition if requested
        If cbAutoSpectr.Checked = True Then
            If SpectraMax.Value >= HighOptimalLevel Or SpectraMax.Value <= LowOptimalLevel Then
                optimumIT(HighOptimalLevel, LowOptimalLevel, 0, 0)
            End If
        End If

        'Save spectrum
        If cbSaveSpectrums.Checked = True Then
            FileNameSpectrum = SpectrumFolder & FileNameGenerator()
            'MsgBox(FileNameSpectrum)
            stringa = FileContentGenerator()
            WriteSpectrumToFile(FileNameSpectrum, stringa)

            SpectrCounter = SpectrCounter + 1
            lSpectrCounter.Text = SpectrCounter.ToString

        End If
        TimerSpectr.Enabled = True
    End Sub

    Public Function GetTemperature(spectrometerIndex As Integer) As Single
        'Read SPECTROMETER temperature
        GetTemperature = 99
        Dim SpectrTemperature As OmniDriver.CCoBoardTemperature    'BoardTemperature
        'If you use wrapper.isFeatureSupportedBoardTemperature(spectrometerIndex) in if clause it doesn't work. It need a boolean variable. Or a conversion!
        IsTemperatureSupported = CBool(wrapper.isFeatureSupportedBoardTemperature(spectrometerIndex))
        If IsTemperatureSupported = True Then
            Try
                SpectrTemperature = wrapper.getFeatureControllerBoardTemperature(spectrometerIndex)
                GetTemperature = CSng(SpectrTemperature.getBoardTemperatureCelsius)
            Catch [error] As Exception
                WriteLogWithTimestamp("Error getting temperature from spectrometer (GetTemperature)")
                WriteLogWithTimestamp([error].ToString)
            End Try
        End If

        Temperature = GetTemperature
        TSSLtemperature.Text = GetTemperature.ToString & "°C " '' & ccdTempUpDown1.Value.ToString
    End Function

    Private Sub GetMinMax()
        Dim i As Integer
        'Very important since they are global variables
        'You MUST reset them!
        SpectraMax.Value = ActualSpectrum(0)
        SpectraMax.Index = 0
        SpectraMin.Value = ActualSpectrum(0)
        SpectraMin.Index = 0
        Dim First As Integer = 1
        Dim Last As Integer = MaxSamples

        If cbRestrictBand.Checked = True Then
            First = iFirstWaveLenghtOptimal
            Last = iSecondWaveLenghtOptimal
        End If

        For i = First To Last
            If ActualSpectrum(i) < SpectraMin.Value Then
                SpectraMin.Value = ActualSpectrum(i)
                SpectraMin.Index = CUInt(i)
            End If
            If ActualSpectrum(i) > SpectraMax.Value Then
                SpectraMax.Value = ActualSpectrum(i)
                SpectraMax.Index = CUInt(i)
            End If
            If SpectraMax.Value > MaxPeak Then
                MaxPeak = SpectraMax.Value
            End If
        Next
        'lMax.Text = SpectraMax.Value.ToString
        'lMin.Text = CStr(0) 'SpectraMin.Value.ToString
        'If cbMaxPeak.Checked Then lMaxPeak.Text = CStr(MaxPeak)
    End Sub

    Public Sub ShiftRatios()
        Dim i As Integer
        For i = 0 To 398
            Ratios(i) = Ratios(i + 1)
        Next
    End Sub

    Public Sub optimumIT(MaxSignal As Single, MinSignal As Single, MaxExpTime As Single, TotalTime As Integer)
        'from long_collection_with_optimum_IT.js
        '//***** JScript for prolonged collection of spectra with automatic  ******************************************
        '//***** determination of optimal integration time when measuring    ******************************************
        '//***** using a Ocean Optics spectrometer and DOASIS                ******************************************
        '//*****                                                             ******************************************
        '//***** Last modified 23/06/2007                                    ******************************************
        '//*****  Author V.I. Tsanev (vip20@cam.ac.uk)                                                            *****



        WriteLogWithTimestamp("OptimumIT starting values Max=" & MaxSignal.ToString & " Min=" & MinSignal.ToString & " ActualMax=" & SpectraMax.Value.ToString & " IntTime=" & IntegrationTime.ToString)

        If debugg = True Then Exit Sub '***********************************?????????????????????????????????????

        Dim DownCoeff As Single = 0.95
        Dim UpCoeff As Single = 1.05
        Dim direction As String

        'Dim iline As Integer
        'Dim metext As Single
        Dim i As Integer = 0
        Dim ExpTime As Single = MaxExpTime
        'Dim ioPro
        'Dim newSpec
        'Dim CurrentMax As Single
        Dim Npixels, Ymax As Single

        Npixels = 2048
        Ymax = 65536
        direction = ""




        wrapper.setScansToAverage(spectrometerIndex, 0)
        ScansToAverage = 0

        lToolStripDebug2.Text = "OPTIMIZING"
        lToolStripDebug2.ForeColor = Color.Red
        LEDSpectrOpt.BackColor = Color.Red

        Do While SpectraMax.Value >= MaxSignal Or SpectraMax.Value <= MinSignal
            i = i + 1
            'iline = i + 4

            spectrometerIndex = 0

            'Set changed parameters
            wrapper.setIntegrationTime(spectrometerIndex, CInt(IntegrationTime))
            'wrapper.setScansToAverage(spectrometerIndex, CInt(ScansToAverage))

            ActualSpectrum = wrapper.getSpectrum(spectrometerIndex)

            GetMinMax()
            'metext = ExpTime 'Maybe I can use only IntegrationTime

            'ScansToAverage ???

            If SpectraMax.Value >= MaxSignal Then
                IntegrationTime = CULng(IntegrationTime * DownCoeff)
                direction = "Down"
            End If
            If SpectraMax.Value <= MinSignal Then
                IntegrationTime = CULng(IntegrationTime * UpCoeff)
                direction = "Up"
            End If

            WriteLogWithTimestamp("ActualMax=" & SpectraMax.Value.ToString & " IntTime=" & IntegrationTime.ToString & " Iteration=" & i.ToString)

            lOptimal.Text = IntegrationTime.ToString
            Application.DoEvents()
            Debug.Print(IntegrationTime.ToString)
            If State = "Stop" Then
                lToolStripDebug2.Text = "STOPPED 1"
                lToolStripDebug2.ForeColor = Color.Black
                WriteLogWithTimestamp("Stop pressed, exiting OptimumIT")
                Exit Sub
            End If

            If cbAutoSpectr.Checked = False Then
                lToolStripDebug2.Text = "STOPPED 2"
                lToolStripDebug2.ForeColor = Color.Black
                WriteLogWithTimestamp("AutoExp for spectrum disabled, exiting OptimumIT")
                Exit Sub
            End If
        Loop

        'NumIntTime.Value = CDec(IntegrationTime / 1000)

        WriteLogWithTimestamp("Integration Time Calculated = " & IntegrationTime.ToString)

        ''Nel codice originale è presente ma non so a cosa serva visto
        ''Che l'integrationtime ottimale è già stato calcolato.
        'If direction = "Down" Then
        '    IntegrationTime = CULng(IntegrationTime * DownCoeff)
        'Else
        '    IntegrationTime = CULng(IntegrationTime * UpCoeff)
        'End If

        If IntegrationTime > MaxIntegrationTime Then
            IntegrationTime = MaxIntegrationTime
        End If
        If IntegrationTime < 1 Then
            IntegrationTime = 1
        End If

        NumIntTime.Value = CDec(IntegrationTime / 1000)
        WriteLogWithTimestamp("Integration effective = " & IntegrationTime.ToString)

        'Qui va fatto il calcolo del NimScans in funzione degli spettri al secondo
        ScansToAverage = CInt(NumExpXsec.Value / (NumIntTime.Value / 1000))
        If ScansToAverage > 200 Then ScansToAverage = 200
        wrapper.setScansToAverage(spectrometerIndex, ScansToAverage)
        NumScans.Value = ScansToAverage
        WriteLogWithTimestamp("NumScans = " & ScansToAverage.ToString & " " & (ScansToAverage * NumIntTime.Value * 1000).ToString)
        lToolStripDebug2.Text = "OK"
        lToolStripDebug2.ForeColor = Color.Black
        LEDSpectrOpt.BackColor = Color.Green
    End Sub

    Public Function FileNameGenerator() As String
        FileNameGenerator = Now.ToUniversalTime().ToString("yyyyMMdd_HHmmss")
        FileNameGenerator &= ".txt"
    End Function

    Public Function FileContentGenerator() As String
        Dim stringa As New System.Text.StringBuilder
        Dim i As Integer
        stringa.AppendLine("USB2000+ Spectrum file")
        stringa.AppendLine(Now.ToUniversalTime().ToString(DateTimeFormat))
        stringa.AppendLine(spectrometerName)
        stringa.AppendLine("Serial #" & spectrometerSerial)
        stringa.AppendLine(Temperature & "°C")
        stringa.AppendLine("Non Linearity Correction = " & NonlinearityCorrection.ToString)
        stringa.AppendLine("Dark Electrical Correction = " & DarkElectricalCorrection.ToString)
        stringa.Append("Coefficients = " & CalIntercept.ToString & "," & CalFirstCoef.ToString & ",")
        stringa.AppendLine(CalSecondCoef & "," & CalThirdCoef.ToString)
        stringa.AppendLine("Integration Time = " & IntegrationTime.ToString)
        stringa.AppendLine("Scans To Average = " & ScansToAverage.ToString)
        stringa.AppendLine("First Wave Lenght = " & FirstWaveLenght.ToString)
        stringa.AppendLine("Second Wave Lenght = " & SecondWaveLenght.ToString)
        stringa.AppendLine("Pan = " & Pan & " Tilt = " & Tilt)
        For i = 0 To MaxIndex
            stringa.AppendLine(WaveLenght(i) & " ; " & ActualSpectrum(i))
        Next
        FileContentGenerator = stringa.ToString
        stringa = Nothing
    End Function

    Public Function WriteSpectrumToFile(Filename As String, testo As String) As Boolean
        WriteSpectrumToFile = False
        'Dim WriteFile As System.IO.File
        Dim WriteObj As System.IO.StreamWriter
        WriteObj = File.CreateText(Filename)
        WriteObj.Write(testo)
        WriteObj.Close()
        WriteSpectrumToFile = True
        'Must add error functions
        'For disk full, Removable media or other type of write errors!
    End Function

    Private Sub bConnectSpectrometer_Click(sender As System.Object, e As System.EventArgs) Handles bConnectSpectrometer.Click
        Dim Success As Boolean = False
        If numberOfSpectrometers <> 0 And bConnect.Text = "Disconnect spectrometer" Then
            wrapper.closeAllSpectrometers()
            bConnect.Text = "Connect spectrometer"
            LEDconnected.BackColor = Color.Red
            lSpectrConnected.Text = "Not Connected"
            gUSB2000.Enabled = False
            DisableButtons()
            WriteLogWithTimestamp("Spectrometer disconnected!")
        Else
            Success = ConnectSpectrometer()
            If Success = True Then
                bConnect.Text = "Disconnect spectrometer"
                gUSB2000.Enabled = True
                WriteLogWithTimestamp("Spectrometer connected!")
            End If
        End If
    End Sub

    Private Function ConnectSpectrometer() As Boolean
        Dim stringa As String

        'Get spectrometer
        'In the example
        'OmniDriver_programming_Manual.pdf chapter 03 page 18
        'Document Number 000-20000-400-12-201208
        'is written that if you have no spectrometer you get -1
        'but I find that instead you get 0 (win 7 64 bit)
        numberOfSpectrometers = wrapper.openAllSpectrometers()
        If numberOfSpectrometers = 0 Then
            ToolStripStatusLabel2.Text = "I/O error occurred, no spectrometers found!"
            MsgBox("I/O error occurred, no spectrometers found!")
            DisableButtons()
            ConnectSpectrometer = False
            Exit Function
        End If

        EnableButtons()

        'Here you works only with first spectrometer found!
        spectrometerIndex = 0
        spectrometerName = "Found " & wrapper.getName(spectrometerIndex)     'Get name of spectrometer
        stringa = spectrometerName
        spectrometerSerial = " serial " & wrapper.getSerialNumber(spectrometerIndex) 'Get serial of spectrometer
        stringa = stringa & spectrometerSerial
        ToolStripStatusLabel2.Text = stringa
        'Turn LED Yellow
        LEDconnected.BackColor = Color.GreenYellow
        lSpectrConnected.Text = "Connected"

        'Check if NonLinearity is supported
        'If wrapper.isFeatureSupportedNonlinearityCorrectionProvider(spectrometerIndex) = CShort(False) Then
        '    chbNonLinearity.Enabled = False
        'End If

        'If wrapper.isFeatureSupportedDarkElectricalCorrection() Then  'This method doesn't exist! So I can't check if is supported!

        'End If
        NonlinearityCorrection = False
        DarkElectricalCorrection = False
        GetTemperature(spectrometerIndex)   'Get internal temperature of spectrometer
        GetCalCoefficients()    'Get calibration coefficients
        BuildWaveLenghts(CalIntercept, CalFirstCoef, CalSecondCoef, CalThirdCoef)   'Build WaveLenghts for X axis
        USB2000chart.Series(0).Points.DataBindXY(WaveLenght, ActualSpectrum)    'Bind spectrum and wavelenghts to graph
        USB2000chart.ChartAreas(0).AxisX.LabelStyle.Format = "#"    'Format X Axis labels to integers
        ConnectSpectrometer = True
    End Function

    Public Function GetCalCoefficients() As Boolean
        Dim coefficients As OmniDriver.CCoCoefficients
        GetCalCoefficients = False
        coefficients = wrapper.getCalibrationCoefficientsFromEEProm(spectrometerIndex)
        CalIntercept = coefficients.getWlIntercept()
        CalFirstCoef = coefficients.getWlFirst()
        CalSecondCoef = coefficients.getWlSecond()
        CalThirdCoef = coefficients.getWlThird()

    End Function

    Public Sub BuildWaveLenghts(Intercept As Double, First As Double, Second As Double, Third As Double)
        Dim i As Integer
        Dim difference As Single
        Dim FirstDifference As Single = 10
        Dim SecondDifference As Single = 10
        'Dim stringa As String
        For i = 0 To MaxSamples
            WaveLenght(i) = Intercept + First * i + Second * i ^ 2 + Third * i ^ 3
            'iWaveLenght(i) = CInt(WaveLenght(i))
            difference = CSng(FirstWaveLenght - WaveLenght(i))
            If Math.Abs(difference) < FirstDifference Then
                FirstDifference = difference
                iFirstWaveLenght = i
            End If
            difference = CSng(SecondWaveLenght - WaveLenght(i))
            If Math.Abs(difference) < SecondDifference Then
                SecondDifference = difference
                iSecondWaveLenght = i
            End If
        Next

        'Idem for finding the indexes of the window for optimal exposition
        FirstDifference = 10
        SecondDifference = 10

        For i = 0 To MaxSamples
            difference = CSng(FirstWaveLenghtOptimal - WaveLenght(i))
            If Math.Abs(difference) < FirstDifference Then
                FirstDifference = difference
                iFirstWaveLenghtOptimal = i
            End If
            difference = CSng(SecondWaveLenghtOptimal - WaveLenght(i))
            If Math.Abs(difference) < SecondDifference Then
                SecondDifference = difference
                iSecondWaveLenghtOptimal = i
            End If
        Next

        If FirstWaveLenghtOptimal = 0 Then
            iFirstWaveLenghtOptimal = 0
            FirstWaveLenghtOptimal = WaveLenght(0)
        End If

        WriteLogWithTimestamp("FirstWavelenght=" & WaveLenght(iFirstWaveLenght))
        WriteLogWithTimestamp("SecondWaveLenght=" & WaveLenght(iSecondWaveLenght))
        WriteLogWithTimestamp("FirstWavelenghtOptimal=" & WaveLenght(iFirstWaveLenghtOptimal))
        WriteLogWithTimestamp("SecondWaveLenghtOptimal=" & WaveLenght(iSecondWaveLenghtOptimal))
    End Sub

    Private Sub bDark_Click(sender As System.Object, e As System.EventArgs) Handles bDark.Click
        'Prende il dark e lo salva con il nome
        If TimerSpectr.Enabled = True Then
            lMessage.Text = "Stop acquisition first!"
            Exit Sub
        End If
        GetSpectrum("_dark.txt")
        lMessage.Text = "Dark taken"
    End Sub

    Public Sub GetSpectrum(postfix As String)
        Dim stringa As String
        ActualSpectrum = wrapper.getSpectrum(spectrometerIndex)
        USB2000chart.Series(0).Points.DataBindXY(WaveLenght, ActualSpectrum)
        GetTemperature(spectrometerIndex)
        GetMinMax()

        FileNameSpectrum = SpectrumFolder & FileNameGenerator()
        FileNameSpectrum = Mid(FileNameSpectrum, 1, Len(FileNameSpectrum) - 4) & postfix
        'MsgBox(FileNameSpectrum)
        stringa = FileContentGenerator()
        WriteSpectrumToFile(FileNameSpectrum, stringa)

    End Sub

    Private Sub bClear_Click(sender As System.Object, e As System.EventArgs) Handles bClear.Click
        'Prende il clear e lo salva con il nome
        If TimerSpectr.Enabled = True Then
            lMessage.Text = "Stop acquisition first!"
            Exit Sub
        End If
        GetSpectrum("_clear.txt")
        lMessage.Text = "Clear taken"
    End Sub

    Private Sub bGetSpectrum_Click(sender As System.Object, e As System.EventArgs) Handles bGetSpectrum.Click
        'Dim stringa As String
        lMessage.Text = ""
        Debug.Print("Get spectrum")
        Debug.WriteLine("Get Spectrum Write")
        ToolStripStatusLabel3.Text = "Getting spectrum..."
        TimeStart = Now
        ActualSpectrum = wrapper.getSpectrum(spectrometerIndex)
        TimeStop = Now
        dAcquisitionTime = CLng(TimeStop.Subtract(TimeStart).TotalMilliseconds)
        ToolStripStatusLabel3.Text = "Got spectrum!"
        USB2000chart.Series(0).Points.DataBindXY(WaveLenght, ActualSpectrum)
        GetTemperature(spectrometerIndex)
        GetMinMax()
        TimeStop = Now
        dAcquisitionTimeTotal = CLng(TimeStop.Subtract(TimeStart).TotalMilliseconds)
        'lAcquisitionTime.Text = dAcquisitionTime.ToString
        'lTotalAcquisitionTime.Text = dAcquisitionTimeTotal.ToString
        'FileNameSpectrum = SpectrumFolder & FileNameGenerator()
        'MsgBox(FileNameSpectrum)
        'stringa = FileContentGenerator()
        'WriteSpectrumToFile(FileNameSpectrum, stringa)
        'MsgBox("Done!")
    End Sub

    Private Sub bContinuous_Click(sender As System.Object, e As System.EventArgs) Handles bContinuous.Click
        TimerSpectr.Enabled = True
        lMessage.Text = ""
        bContinuous.BackColor = Color.Red
        State = "Run"
        WriteLogWithTimestamp("Starting spectrums acquisition")
    End Sub

    Private Sub bStopSpectrAcq_Click(sender As System.Object, e As System.EventArgs) Handles bStopSpectrAcq.Click
        TimerSpectr.Enabled = False
        bContinuous.BackColor = ColorControl
        State = "Stop"
        WriteLogWithTimestamp("Stopping spectrums acquisition")
    End Sub
    Private Sub NumIntTime_ValueChanged(sender As System.Object, e As System.EventArgs) Handles NumIntTime.ValueChanged
        IntegrationTime = CULng(CULng(NumIntTime.Value) * 1000)
        Try
            wrapper.setIntegrationTime(spectrometerIndex, CInt(IntegrationTime))
        Catch [error] As Exception
            WriteLogWithTimestamp("Error on setting integration time on spectrometer!")
            WriteLogWithTimestamp([error].ToString)
            Exit Sub
        End Try
        WriteLogWithTimestamp("Integration time=" & IntegrationTime.ToString)
    End Sub

    Private Sub NumScans_ValueChanged(sender As System.Object, e As System.EventArgs) Handles NumScans.ValueChanged
        Try
            wrapper.setScansToAverage(spectrometerIndex, CInt(NumScans.Value))
        Catch [error] As Exception
            WriteLogWithTimestamp("Error on setting ScansToAverage on spectrometer!")
            WriteLogWithTimestamp([error].ToString)
            Exit Sub
        End Try
        ScansToAverage = CUInt(NumScans.Value)
        WriteLogWithTimestamp("Scans to average=" & ScansToAverage.ToString)
    End Sub
    Private Sub cbSaveSpectrums_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbSaveSpectrums.CheckedChanged
        If cbSaveSpectrums.Checked = True Then
            lSpectrRecording.Text = "Recording"
            lSpectrRecording.ForeColor = Color.Red
            WriteLogWithTimestamp("Starting spectrums recording!")
        Else
            lSpectrRecording.Text = "Not Recording"
            lSpectrRecording.ForeColor = Color.LimeGreen
            WriteLogWithTimestamp("Stopping spectrums recording!")
            WriteLogWithTimestamp("Taken " & lSpectrCounter.Text & " spectrums")
        End If
    End Sub

    Private Sub DoAutomaticAcquisition()
        WriteLogWithTimestamp("Starting Automatic acquisition")
        Me.Show()
        Application.DoEvents()

        Dim CamerasOK As Boolean = False
        Dim SpectrometerOK As Boolean = False

        'Disable some elements in GUI and modify others
        YellowLinePanel1.Visible = False
        YellowLinePanel2.Visible = False
        VerticalLine2.Visible = False
        If UseMatrix = True Then
            Me.Text = "JAIspectr01 - AUTOMATIC MODE - USE Matrix"
        Else
            Me.Text = "JAIspectr01 - AUTOMATIC MODE"
        End If

        'GoTo SaltaCam
        cbAutoExp.Checked = True
        cbUsePeak.Checked = True
        cbLongExp.Checked = True

        'Connect Cams
        WriteLogWithTimestamp("Connecting cameras")
        IsVideo = False
        StartExposition()
        Application.DoEvents()
        bConnect.Text = "Disconnect"
        EnableCamButtons()
        connected = True
        WriteLogWithTimestamp("Cameras connected")



        Application.DoEvents()

        'If cam1.Connected = True And cam2.Connected = True Then
        '    CamerasOK = True
        'End If
        Application.DoEvents()

SaltaCam:

        'Connect spectrometer
        Dim Success As Boolean = False
        numberOfSpectrometers = wrapper.openAllSpectrometers()
        If numberOfSpectrometers <> 0 Then
            Success = ConnectSpectrometer()

            If Success = True Then
                bConnect.Text = "Disconnect spectrometer"
                gUSB2000.Enabled = True
                WriteLogWithTimestamp("Spectrometer connected!")
                SpectrometerOK = True
            Else
                WriteLogWithTimestamp("Cannot connect to spectrometer found!")
                End
            End If
        Else
            WriteLogWithTimestamp("No spectrometer found!")
            End
        End If

        Application.DoEvents()

        If Not debugg Then
            cbAutoSpectr.Checked = True
            WriteLogWithTimestamp("Auto setting for spectrometer activated")
            cbSaveSpectrums.Checked = True
        End If


        'If debugg = True Then cbAutoSpectr.Checked = False
        'If debugg = True Then cbSaveSpectrums.Checked = False


        cbRestrictBand.Checked = True
        WriteLogWithTimestamp("Restrict band activated")

        'setpointTrackBar.Value = BestIntensity '58000
        m_ALCController.SetSetPoint(setpointTrackBar.Value)
        setpointTextBox.Text = setpointTrackBar.Value.ToString()

        Application.DoEvents()

        'Start spectrometer acquisition
        TimerSpectr.Enabled = True
        lMessage.Text = ""
        bContinuous.BackColor = Color.Red
        State = "Run"
        bStopSpectrAcq.Enabled = True
        WriteLogWithTimestamp("Starting spectrums acquisition")

        'GoTo SaltaCamAcquisition



        Application.DoEvents()


        cbAutoExp.Checked = True
        cbUsePeak.Checked = True
        If Not debugg Then cbSave.Checked = True
        WriteLogWithTimestamp("Cameras auto exposure is on")

        'PROBLEM: It is better to take dark after reaching best exposition *******************************
        'WriteLogWithTimestamp("Getting cameras dark")
        'lStatus.Text = "Stop"
        'GetCameraDark()
        'WriteLogWithTimestamp("Cameras dark taken")

        Station = txStation_Text

        WriteLogWithTimestamp("DoAutomaticAcquisition - Starting camera acquisition")

        'Start cam acquisition

        Running = True
        lStatus.Text = "Run"
        lStatus.ForeColor = Color.Red
        bAutoAcq.BackColor = Color.Red
        bAutoAcq.Enabled = False
        StopButton.Enabled = True

        'WriteLogWithTimestamp("Starting camera acquisition")

        myCamera1.GetNode("AcquisitionMode").Value = "SingleFrame"
        myCamera2.GetNode("AcquisitionMode").Value = "SingleFrame"

        myCamera1.GetNode("TriggerSelector").Value = "FrameStart"
        myCamera1.GetNode("TriggerMode").Value = "Off"
        'myCamera1.GetNode("ExposureMode").Value = "Continuous trigger"
        myCamera1.GetNode("ExposureMode").Value = "Continuous"

        myCamera2.GetNode("TriggerSelector").Value = "FrameStart"
        myCamera2.GetNode("TriggerMode").Value = "Off"
        'myCamera1.GetNode("ExposureMode").Value = "Continuous trigger"
        myCamera2.GetNode("ExposureMode").Value = "Continuous"


        Timer1.Enabled = True
        TimerAcq.Enabled = True
        LtimerAcq_Text = "tmrAcq running"
        LtimerAcq.Text = "tmrAcq running"
        Ltimer1.Text = "tmr1 running"
        Ltimer1_Text = "tmr1 running"

        If debugg = True Then
            TimerDebug.Enabled = True
        Else
            Ltimer1.Text = ""
            LtimerAcq.Text = ""
            LtimerAcq_Text = ""
            Ltimer1_Text = ""

        End If

    End Sub

    Private Sub TimerDebug_Tick(sender As System.Object, e As System.EventArgs) Handles TimerDebug.Tick
        LtimerAcq.Text = LtimerAcq_Text
        Ltimer1.Text = Ltimer1_Text
        lDebug3.Text = TempoEsposizione
        lDebug2.Text = (MaxIntensityy * 64).ToString
        lDebug4.Text = MaxIntensityy
        ldebug.Text = maxx

        lDebug5.Text = MaxHistogramIndex
    End Sub
End Class
Public Class ALC

    Public Enum EALCType

        MinGainPriority = 0

        MinExposurePriority = 1

        GainPriority = 2

        ExposurePriority = 3

        GainOnly = 4

        ExposureOnly = 5

        Undefined
    End Enum

    Private m_ALCType As EALCType = EALCType.MinGainPriority

    ' Type of ALC control
    Private m_MinGain As Integer

    ' Minimum Gain value
    Private m_MaxGain As Integer

    ' Maximum Gain value
    Private m_CurrentGain As Integer

    ' Current Gain value
    Private m_MinExposure As Integer

    ' Minimum Exposure Time value
    Private m_MaxExposure As Integer

    ' Maximum Exposure Time value
    Private m_CurrentExposure As Integer

    ' Current Exposure Time value
    Private m_SetPoint As Integer

    ' This is the target average for the Closed-Loop control
    Private m_Average As Integer

    ' This is the last known average input for the Closed-Loop control
    Private m_KGain As Double

    ' Gain for the Gain adjustments
    Private m_KExposure As Double

    ' Gain for the Exposure Time adjustments
    Private m_Threshold As Double

    ' Threshold value for the error
    Private Function ReCalculate() As Boolean
        Return Calculate(m_Average)
    End Function

    Public Function Calculate(ByVal input As Integer) As Boolean
        m_Average = input
        ' We can now continue with the actual Closed-Loop calculations
        Dim relativeError As Double = (m_SetPoint - input) / m_SetPoint
        Dim changes As Boolean = False
        Select Case (m_ALCType)
            Case EALCType.MinGainPriority
                ' We typically only want to adjust one of the camera settings at a time
                ' and typically it makes sense to minimize the Gain in order to get as
                ' little noise as possible in the image.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    ' As long as we can expose longer then we want to force the Gain to a minimum!
                    If ((m_CurrentExposure < m_MaxExposure) _
                                AndAlso (m_CurrentGain > m_MinGain)) Then
                        changes = AdjustGain(((relativeError / 2) _
                                        * -1))
                        changes = (changes Or AdjustExposureTime((relativeError * 2)))
                    ElseIf (m_CurrentExposure = m_MaxExposure) Then
                        changes = AdjustGain(relativeError)
                    Else
                        changes = AdjustExposureTime(relativeError)
                    End If
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    ' As long as we can expose shorter then we want to force the Gain to a minimum!
                    If (m_CurrentGain > m_MinGain) Then
                        changes = AdjustGain(relativeError)
                    Else
                        changes = AdjustExposureTime(relativeError)
                    End If
                ElseIf ((m_CurrentGain > m_MinGain) _
                            AndAlso (m_CurrentExposure < m_MaxExposure)) Then
                    If (m_CurrentGain > m_MinGain) Then
                        changes = AdjustGain(-0.1)
                    End If
                    If (changes _
                                AndAlso (m_CurrentExposure < m_MaxExposure)) Then
                        changes = AdjustExposureTime(0.1)
                    End If
                End If
            Case EALCType.MinExposurePriority
                ' We typically only want to adjust one of the camera settings at a time
                ' and typically it makes sense to minimize the Gain in order to get as
                ' little noise as possible in the image.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    ' As long as we can increase Gain then we want to force the Exposure Time to a minimum!
                    If ((m_CurrentGain < m_MaxGain) _
                                AndAlso (m_CurrentExposure > m_MinExposure)) Then
                        changes = AdjustExposureTime(((relativeError / 2) _
                                        * -1))
                        changes = (changes Or AdjustGain((relativeError * 2)))
                    ElseIf (m_CurrentGain = m_MaxGain) Then
                        changes = AdjustExposureTime(relativeError)
                    Else
                        changes = AdjustGain(relativeError)
                    End If
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    ' As long as we can expose shorter then we want to force the Gain to a minimum!
                    If (m_CurrentExposure > m_MinExposure) Then
                        changes = AdjustExposureTime(relativeError)
                    Else
                        changes = AdjustGain(relativeError)
                    End If
                ElseIf ((m_CurrentExposure > m_MinExposure) _
                            AndAlso (m_CurrentGain < m_MaxGain)) Then
                    If (m_CurrentExposure > m_MinExposure) Then
                        changes = AdjustExposureTime(-0.1)
                    End If
                    If (changes _
                                AndAlso (m_CurrentGain < m_MaxGain)) Then
                        changes = AdjustGain(0.1)
                    End If
                End If
            Case EALCType.GainPriority
                ' In this mode we try to adjust gain until it reaches one of the limits and then we adjust the exposure time.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    ' As long as we can increase Gain then we do it!
                    If (m_CurrentGain < m_MaxGain) Then
                        changes = AdjustGain(relativeError)
                    Else
                        changes = AdjustExposureTime(relativeError)
                    End If
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    ' As long as we can decrease Gain then we do it!
                    If (m_CurrentGain > m_MinGain) Then
                        changes = AdjustGain(relativeError)
                    Else
                        changes = AdjustExposureTime(relativeError)
                    End If
                End If
            Case EALCType.ExposurePriority
                ' In this mode we try to adjust exposure time until it reaches one of the limits and then we adjust the gain.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    ' As long as we can increase exposure time then we do it!
                    If (m_CurrentExposure < m_MaxExposure) Then
                        changes = AdjustExposureTime(relativeError)
                    Else
                        changes = AdjustGain(relativeError)
                    End If
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    ' As long as we can decrease exposure time then we do it!
                    If (m_CurrentExposure > m_MinExposure) Then
                        changes = AdjustExposureTime(relativeError)
                    Else
                        changes = AdjustGain(relativeError)
                    End If
                End If
            Case EALCType.GainOnly
                ' In this mode we only try to adjust the gain.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    changes = AdjustGain(relativeError)
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    changes = AdjustGain(relativeError)
                End If
            Case EALCType.ExposureOnly
                ' In this mode we only try to adjust the exposure time.
                ' Do we need to increase Gain/Exposure time?
                If (relativeError > m_Threshold) Then
                    changes = AdjustExposureTime(relativeError)
                ElseIf (relativeError _
                            < (m_Threshold * -1)) Then
                    changes = AdjustExposureTime(relativeError)
                End If
        End Select
        Return changes
    End Function

    ' Set the type of ALC control
    Public Function SetALCType(ByVal type As EALCType) As Boolean
        m_ALCType = type
        Return ReCalculate
    End Function

    ' Set target for the closed-loop control
    Public Function SetSetPoint(ByVal setpoint As Integer) As Boolean
        m_SetPoint = setpoint
        Return ReCalculate
    End Function

    ' Set the min gain for the closed-loop control
    Public Function SetMinGain(ByVal min As Integer) As Boolean
        Dim changes As Boolean = False
        m_MinGain = min
        ' When we adjust the Min gain then we should adjust the current gain accordingly in case the
        ' current gain is outside the new boundries
        If (m_CurrentGain < m_MinGain) Then
            m_CurrentGain = m_MinGain
            changes = True
        End If
        Return changes
    End Function

    ' Set the max gain for the closed-loop control
    Public Function SetMaxGain(ByVal max As Integer) As Boolean
        Dim changes As Boolean = False
        m_MaxGain = max
        ' When we adjust the Min gain then we should adjust the current gain accordingly in case the
        ' current gain is outside the new boundries
        If (m_CurrentGain > m_MaxGain) Then
            m_CurrentGain = m_MaxGain
            changes = True
        End If
        Return changes
    End Function

    ' Set the min exposure for the closed-loop control
    Public Function SetMinExposure(ByVal min As Integer) As Boolean
        Dim changes As Boolean = False
        m_MinExposure = min
        ' When we adjust the Min gain then we should adjust the current gain accordingly in case the
        ' current gain is outside the new boundries
        If (m_CurrentExposure < m_MinExposure) Then
            m_CurrentExposure = m_MinExposure
            changes = True
        End If
        Return changes
    End Function

    ' Set the max gain for the closed-loop control
    Public Function SetMaxExposure(ByVal max As Integer) As Boolean
        Dim changes As Boolean = False
        m_MaxExposure = max
        ' When we adjust the Min gain then we should adjust the current gain accordingly in case the
        ' current gain is outside the new boundries
        If (m_CurrentExposure > m_MaxExposure) Then
            m_CurrentExposure = m_MaxExposure
            changes = True
        End If
        Return changes
    End Function

    ' Initial gain value
    Public Function SetCurrentGain(ByVal gain As Integer) As Boolean
        m_CurrentGain = gain
        Return True
    End Function

    ' Initial exposure value
    Public Function SetCurrentExposure(ByVal exposure As Integer) As Boolean
        m_CurrentExposure = exposure
        Return True
    End Function

    ' Relaive threshold value around setpoint
    Public Function SetThreshold(ByVal threshold As Double) As Boolean
        m_Threshold = threshold
        Return ReCalculate
    End Function

    ' Set closed-loop "Gain" for the Gain
    Public Function SetKGain(ByVal kGain As Double) As Boolean
        m_KGain = kGain
        Return ReCalculate
    End Function

    ' Set closed-loop "Gain" for the Exposure
    Public Function SetKExposure(ByVal kExposure As Double) As Boolean
        m_KExposure = kExposure
        Return ReCalculate
    End Function

    ' Get the current Gain value
    Public Function GetCurrentGain() As Integer
        Return m_CurrentGain
    End Function

    ' Get the current Exposure value
    Public Function GetCurrentExposure() As Integer
        Return m_CurrentExposure
    End Function

    Public Function GetALCType() As EALCType
        Return m_ALCType
    End Function

    Public Function GetSetPoint() As Integer
        Return m_SetPoint
    End Function

    Public Function GetMinGain() As Integer
        Return m_MinGain
    End Function

    Public Function GetMaxGain() As Integer
        Return m_MaxGain
    End Function

    Public Function GetMinExposure() As Integer
        Return m_MinExposure
    End Function

    Public Function GetMaxExposure() As Integer
        Return m_MaxExposure
    End Function

    Public Function GetThreshold() As Double
        Return m_Threshold
    End Function

    Public Function GetKGain() As Double
        Return m_KGain
    End Function

    Public Function GetKExposure() As Double
        Return m_KExposure
    End Function

    Private Function AdjustGain(ByVal errore As Double) As Boolean
        Dim changes As Boolean = False
        'Dim newGain As Integer = (m_CurrentGain + CType((m_KGain * errore), Integer))
        Dim newGain As Integer = m_CurrentGain + (m_KGain * errore)
        ' Limit the range of the Gain value
        If (newGain > m_MaxGain) Then
            newGain = m_MaxGain
        End If
        If (newGain < m_MinGain) Then
            newGain = m_MinGain
        End If
        ' Did we change gain?
        If (newGain <> m_CurrentGain) Then
            m_CurrentGain = newGain
            changes = True
        End If
        Return changes
    End Function

    Private Function AdjustExposureTime(ByVal errore As Double) As Boolean
        Dim changes As Boolean = False
        'Dim newExposureTime As Integer = (m_CurrentExposure + CType((m_KExposure  _
        '            * (error * CType((Math.Log(CType(m_CurrentExposure,Double)) / Math.Log(2)),Double))),Integer))

        Dim newExposureTime As Integer = m_CurrentExposure + (100 * m_KExposure * errore * (Math.Log(m_CurrentExposure) / Math.Log(2.0)))
        ' Check to see if the values are close to minimum
        ' Limit the range of the Exposure value
        If (newExposureTime > m_MaxExposure) Then
            newExposureTime = m_MaxExposure
        End If
        If (newExposureTime < m_MinExposure) Then
            newExposureTime = m_MinExposure
        End If
        ' Did we change Exposure?
        If (newExposureTime <> m_CurrentExposure) Then
            m_CurrentExposure = newExposureTime
            changes = True
        End If
        Return changes
    End Function
End Class
