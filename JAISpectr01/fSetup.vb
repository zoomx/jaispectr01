﻿Public Class fSetup
    Private myCameraTemp As Jai_FactoryDotNET.CCamera
    Private CameraName(10) As String
    Private CameraFilter(10) As String
    Private CameraIndex As Integer = 1
    Private Selections As Integer = 0

    Private i310 As Integer = 999
    Private i330 As Integer = 999
    Private Sub fSetup_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim nCameras As Integer

        fMain.myFactory.UpdateCameraList(Jai_FactoryDotNET.CFactory.EDriverType.FilterDriver)
        nCameras = fMain.myFactory.CameraList.Count

        If (fMain.myFactory.CameraList.Count >= 2) Then
            For i = 0 To fMain.myFactory.CameraList.Count
                myCameraTemp = fMain.myFactory.CameraList(i)
                ListBox1.Items.Add(myCameraTemp.SerialNumber.ToString)
                myCameraTemp = Nothing
            Next
        Else
            MsgBox("There are only " & nCameras.ToString & "cameras!")

        End If
        bOK.Enabled = False
    End Sub

    Private Sub bAbort_Click(sender As System.Object, e As System.EventArgs) Handles bAbort.Click
        Me.Close()
        fMain.Show()
    End Sub

    Private Sub ListBox1_DoubleClick(sender As Object, e As System.EventArgs) Handles ListBox1.DoubleClick
        Dim response As Integer
        Dim Index As Integer = 0
        response = MsgBox("Is this camera the one with 310 filter?", MsgBoxStyle.YesNoCancel)
        Index = ListBox1.SelectedIndex
        If response = 2 Then Exit Sub '2=Cancel
        If response = 6 Then  '6=Yes
            CameraFilter(Index) = "310"
            i310 = Index
            Selections += 1
        Else
            CameraFilter(Index) = "330"
            Selections += 1
            i330 = Index
        End If

        If Selections >= 2 Then
            bOK.Enabled = True
        End If
    End Sub

    Private Sub bOK_Click(sender As System.Object, e As System.EventArgs) Handles bOK.Click
        Dim stringa As String
        Dim response As Integer
        If i330 = 999 Or i310 = 999 Then
            MsgBox("You must associate a camera to it's filter!")
            Exit Sub
        End If
        stringa = ListBox1.Items(i310).ToString & "  is  310" & vbCrLf
        stringa &= ListBox1.Items(i330).ToString & "  is  330" & vbCrLf
        stringa &= vbCrLf & "Is that correct?"
        response = MsgBox(stringa, MsgBoxStyle.YesNo)
        If response = 7 Then '7=No
            Exit Sub
        End If

        fMain.camSerial(1) = ListBox1.Items(i310).ToString
        fMain.Cam1SerialExpected = fMain.camSerial(1)

        fMain.camSerial(2) = ListBox1.Items(i330).ToString
        fMain.Cam2SerialExpected = fMain.camSerial(2)

        fMain.SearchButton_Click(Nothing, Nothing)

        MsgBox("Remember to save settings!")

        Me.Close()
        fMain.Show()
    End Sub
End Class