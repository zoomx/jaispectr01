﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fSetup
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rb330 = New System.Windows.Forms.RadioButton()
        Me.rb310 = New System.Windows.Forms.RadioButton()
        Me.bOK = New System.Windows.Forms.Button()
        Me.bAbort = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(12, 12)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(141, 56)
        Me.ListBox1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rb330)
        Me.GroupBox1.Controls.Add(Me.rb310)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(70, 63)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filters"
        Me.GroupBox1.Visible = False
        '
        'rb330
        '
        Me.rb330.AutoSize = True
        Me.rb330.Location = New System.Drawing.Point(6, 40)
        Me.rb330.Name = "rb330"
        Me.rb330.Size = New System.Drawing.Size(43, 17)
        Me.rb330.TabIndex = 1
        Me.rb330.TabStop = True
        Me.rb330.Text = "330"
        Me.rb330.UseVisualStyleBackColor = True
        '
        'rb310
        '
        Me.rb310.AutoSize = True
        Me.rb310.Location = New System.Drawing.Point(6, 19)
        Me.rb310.Name = "rb310"
        Me.rb310.Size = New System.Drawing.Size(43, 17)
        Me.rb310.TabIndex = 0
        Me.rb310.TabStop = True
        Me.rb310.Text = "310"
        Me.rb310.UseVisualStyleBackColor = True
        '
        'bOK
        '
        Me.bOK.Enabled = False
        Me.bOK.Location = New System.Drawing.Point(88, 105)
        Me.bOK.Name = "bOK"
        Me.bOK.Size = New System.Drawing.Size(50, 32)
        Me.bOK.TabIndex = 54
        Me.bOK.Text = "OK"
        Me.bOK.UseVisualStyleBackColor = True
        '
        'bAbort
        '
        Me.bAbort.Location = New System.Drawing.Point(144, 105)
        Me.bAbort.Name = "bAbort"
        Me.bAbort.Size = New System.Drawing.Size(50, 32)
        Me.bAbort.TabIndex = 55
        Me.bAbort.Text = "Abort"
        Me.bAbort.UseVisualStyleBackColor = True
        '
        'fSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(222, 162)
        Me.Controls.Add(Me.bAbort)
        Me.Controls.Add(Me.bOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "fSetup"
        Me.Text = "fSetup"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rb330 As System.Windows.Forms.RadioButton
    Friend WithEvents rb310 As System.Windows.Forms.RadioButton
    Private WithEvents bOK As System.Windows.Forms.Button
    Private WithEvents bAbort As System.Windows.Forms.Button
End Class
